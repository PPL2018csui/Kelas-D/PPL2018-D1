package com.example.windows81.kugen.activity;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.speech.RecognizerIntent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.windows81.kugen.R;

import java.util.ArrayList;

public class RespondentActivity extends AppCompatActivity {
    private EditText nameField;
    private EditText ageField;
    private EditText addressField;
    private EditText educationField;
    private RadioGroup genderField;
    private Button btn_submit;
    private Button btn_record;
    private TextView speech_text;
    private String survey_id;
    private final int REQ_SPEECH_CODE = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_respondent);

        nameField = findViewById(R.id.respondent_name);
        ageField = findViewById(R.id.respondent_age);
        addressField = findViewById(R.id.respondent_address);
        educationField = findViewById(R.id.respondent_education);
        genderField = findViewById(R.id.respondent_gender);
        speech_text = findViewById(R.id.record_status);
        btn_submit = findViewById(R.id.btn_submit);
        btn_record = findViewById(R.id.btn_record_audio);

        btn_record.setOnClickListener(micClickListener);

        nameField.addTextChangedListener (new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
            @Override
            public void onTextChanged(CharSequence s, int i, int i1, int i2){
                validateForm();
            }
            @Override
            public void afterTextChanged(Editable editable) {}
        });
        ageField.addTextChangedListener (new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
            @Override
            public void onTextChanged(CharSequence s, int i, int i1, int i2){
                validateForm();
            }
            @Override
            public void afterTextChanged(Editable editable) {}
        });
        genderField.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                validateForm();
            }
        });

        addressField.addTextChangedListener (new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
            @Override
            public void onTextChanged(CharSequence s, int i, int i1, int i2){
                validateForm();
            }
            @Override
            public void afterTextChanged(Editable editable) {}
        });
        educationField.addTextChangedListener (new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
            @Override
            public void onTextChanged(CharSequence s, int i, int i1, int i2){
                validateForm();
            }
            @Override
            public void afterTextChanged(Editable editable) {}
        });
    }

    private View.OnClickListener micClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v){
            btnToOpenMic();
        }
    };

    private void btnToOpenMic(){
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, "id-ID");
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "BACA: Saya setuju melakukan survei ini");
        Log.d("open mic", "try");
        try {
            startActivityForResult(intent, REQ_SPEECH_CODE);
            Log.d("success", "success");
        } catch (ActivityNotFoundException a){
            Toast.makeText(RespondentActivity.this, "Sorry device does not support", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("masuk bro", "adas");
        switch (requestCode) {
            case REQ_SPEECH_CODE: {
                if (resultCode == RESULT_OK && null != data) {
                    Log.d("result_ok", resultCode+"");
                    ArrayList<String> voiceInText = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    speech_text.setText(voiceInText.get(0));
                    validateForm();
                } else {
                    Log.d("result_fail", requestCode+"");
                }
                break;
            }
        }
    }

    public void onSubmit(View v) {
        Intent intent = new Intent(this, QuestionActivity.class);

        int selectedId = genderField.getCheckedRadioButtonId();
        RadioButton selectedGender = (RadioButton)findViewById(selectedId);
        survey_id = getIntent().getStringExtra("survey_id");

        intent.putExtra("name_respondent", nameField.getText().toString());
        intent.putExtra("age_respondent", ageField.getText().toString());
        intent.putExtra("gender_respondent", selectedGender.getText().toString());
        intent.putExtra("domicile_respondent", addressField.getText().toString());
        intent.putExtra("education_respondent", educationField.getText().toString());
        intent.putExtra("survey_id", survey_id);
        startActivity(intent);
    }

//    setButton (btnSurvey, getIntent().getStringExtra("survey_id"));
//
//    private void setButton(Button btn, final String survey_id) {
//        btn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(DetailActivity.this, QuestionActivity.class);
//                intent.putExtra("survey_id",survey_id);
//                startActivity(intent);
//            }
//        });
//    }

    private void validateForm(){
        boolean isNameNull = nameField.getText().toString().isEmpty();
        boolean isAddressNull = addressField.getText().toString().isEmpty();
        boolean isEducationNull = educationField.getText().toString().isEmpty();
        boolean isAgeNull = ageField.getText().toString().isEmpty();
        boolean isSpeechTextValidated = speech_text.getText().toString().equalsIgnoreCase("Saya setuju melakukan survei ini");
        int isChecked = genderField.getCheckedRadioButtonId();

        if (!(isNameNull || isAddressNull || isEducationNull || isAgeNull) && isSpeechTextValidated && (isChecked > 0)) {
            btn_submit.setBackground(getDrawable(R.drawable.blue_button));
            btn_submit.setEnabled(true);
            btn_submit.setClickable(true);
        } else {
            btn_submit.setBackground(getDrawable(R.drawable.disabled_button));
            btn_submit.setEnabled(false);
            btn_submit.setClickable(false);
        }
    }
}
