package com.example.windows81.kugen.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.windows81.kugen.model.OptionItem;
import com.example.windows81.kugen.model.SurveyQuestion;
import com.example.windows81.kugen.schema.OptionItemSchema;

import java.util.Map;
import java.util.TreeMap;

/**
 * Created by rifkiadrn on 4/21/2018.
 */

public class OptionItemHandler extends DatabaseHandler{
    private SQLiteDatabase db;

    public OptionItemHandler(Context context) {
        super(context);
        this.db = super.getDatabase();
    }

    public void addOption(String survey_id, String question_id, String option_id, OptionItem oi){
        db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(OptionItemSchema.OptionItemTable.COLUMN_NAME_QUESTIONID, question_id);
        values.put(OptionItemSchema.OptionItemTable.COLUMN_NAME_SURVEYID, survey_id);
        values.put(OptionItemSchema.OptionItemTable.COLUMN_NAME_OPTIONID, option_id);
        values.put(OptionItemSchema.OptionItemTable.COLUMN_NAME_OPTION, oi.getOptionAnswer());
        values.put(OptionItemSchema.OptionItemTable.COLUMN_NAME_OPTION_ORDER, oi.getOrder());
        values.put(OptionItemSchema.OptionItemTable.COLUMN_NAME_ASSESSMENT_VALUE, oi.getAssessmentValue());

        db.insert(OptionItemSchema.OptionItemTable.TABLE_NAME, null, values);

    }
    public void addAllOption(SurveyQuestion sq) {
        for (String option_id: sq.getOptionItems().keySet()){
            if(!isContains(sq.getSurvey_id(), sq.getQuestion_id(), option_id)){
                addOption(sq.getSurvey_id(), sq.getQuestion_id(), option_id, sq.getOptionItems().get(option_id));
            } else {
                updateOption(sq.getSurvey_id(), sq.getQuestion_id(), option_id, sq.getOptionItems().get(option_id));
            }
        }
    }

    public Map<String, OptionItem> getAllOption(SurveyQuestion sq) {
        db = getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT * FROM " + OptionItemSchema.OptionItemTable.TABLE_NAME +" WHERE "+ OptionItemSchema.OptionItemTable.COLUMN_NAME_SURVEYID +"='" + sq.getSurvey_id() + "'"+" AND "+ OptionItemSchema.OptionItemTable.COLUMN_NAME_QUESTIONID +"='" +sq.getQuestion_id() +"'"+ " ORDER BY " + OptionItemSchema.OptionItemTable.COLUMN_NAME_OPTION_ORDER + " ASC", null);

        Map<String, OptionItem> optionList = new TreeMap<>();

        // Traversing through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                OptionItem optionItem = new OptionItem();
                optionItem.setOptionAnswer(cursor.getString(cursor.getColumnIndex(OptionItemSchema.OptionItemTable.COLUMN_NAME_OPTION)));
                optionItem.setOrder(cursor.getInt(cursor.getColumnIndex(OptionItemSchema.OptionItemTable.COLUMN_NAME_OPTION_ORDER)));
                optionItem.setAssessmentValue(cursor.getInt(cursor.getColumnIndex(OptionItemSchema.OptionItemTable.COLUMN_NAME_ASSESSMENT_VALUE)));
                String option_id = cursor.getString(cursor.getColumnIndex(OptionItemSchema.OptionItemTable.COLUMN_NAME_OPTIONID));

                optionList.put(option_id, optionItem);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        Log.d("size db option", optionList.toString());
        return optionList;
    }

    public OptionItem getOption(String survey_id, String question_id, String option_id) {
        db = getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT * FROM " + OptionItemSchema.OptionItemTable.TABLE_NAME +" WHERE "+ OptionItemSchema.OptionItemTable.COLUMN_NAME_SURVEYID +"='" + survey_id + "'"+" AND "+ OptionItemSchema.OptionItemTable.COLUMN_NAME_QUESTIONID +"='" +question_id +"'" +" AND "+ OptionItemSchema.OptionItemTable.COLUMN_NAME_OPTIONID +"='" +option_id +"'"+ " ORDER BY " + OptionItemSchema.OptionItemTable.COLUMN_NAME_OPTION_ORDER + " ASC", null);
        OptionItem item = null;
        if (cursor.moveToFirst()){
            item = new OptionItem();
            item.setOptionAnswer(cursor.getString(cursor.getColumnIndex(OptionItemSchema.OptionItemTable.COLUMN_NAME_OPTION)));
            item.setOrder(cursor.getInt(cursor.getColumnIndex(OptionItemSchema.OptionItemTable.COLUMN_NAME_OPTION_ORDER)));
            item.setAssessmentValue(cursor.getInt(cursor.getColumnIndex(OptionItemSchema.OptionItemTable.COLUMN_NAME_ASSESSMENT_VALUE)));
            Log.d("SET ASSESSMENT_VALUE", item.getOptionAnswer() + "---" + item.getAssessmentValue());
        }
        cursor.close();
        db.close();
        return item;
    }

    public boolean isContains(String survey_id, String question_id, String option_id) {
        db = getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT * FROM " + OptionItemSchema.OptionItemTable.TABLE_NAME +" WHERE "+ OptionItemSchema.OptionItemTable.COLUMN_NAME_SURVEYID +"='" + survey_id + "'"+" AND "+ OptionItemSchema.OptionItemTable.COLUMN_NAME_QUESTIONID +"='" +question_id +"'"+ " AND "+ OptionItemSchema.OptionItemTable.COLUMN_NAME_OPTIONID +"='" +option_id +"'"+ " ORDER BY " + OptionItemSchema.OptionItemTable.COLUMN_NAME_OPTION_ORDER + " ASC", null);
        if (cursor.getCount() > 0) {
            return true;
        }
        return false;
    }

    public void deleteOption(String survey_id, String question_id, String option_id) {
        db = getWritableDatabase();
        if (isContains(survey_id,question_id,option_id)) {
            db.delete(OptionItemSchema.OptionItemTable.TABLE_NAME, OptionItemSchema.OptionItemTable.COLUMN_NAME_SURVEYID +"='" + survey_id + "'"+" AND "+ OptionItemSchema.OptionItemTable.COLUMN_NAME_QUESTIONID +"='" +question_id +"'"+ " AND "+ OptionItemSchema.OptionItemTable.COLUMN_NAME_OPTIONID +"='" +option_id +"'", null);
            db.close();
        }
    }

    public void deleteAllOption(SurveyQuestion sq) {
        db = getWritableDatabase();
        for (String option_id: sq.getOptionItems().keySet()){
            if (isContains(sq.getSurvey_id(), sq.getQuestion_id(), option_id)){
                deleteOption(sq.getSurvey_id(), sq.getQuestion_id(), option_id);
            }
        }
    }

    public void deleteOptionBasedOnSurveyId(String survey_id){
        db = getWritableDatabase();
        db.delete(OptionItemSchema.OptionItemTable.TABLE_NAME, OptionItemSchema.OptionItemTable.COLUMN_NAME_SURVEYID+"='"+survey_id+"'", null);
        db.close();
    }

    public void updateOption(String survey_id, String question_id, String option_id, OptionItem newItem){
        db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(OptionItemSchema.OptionItemTable.COLUMN_NAME_SURVEYID, survey_id);
        values.put(OptionItemSchema.OptionItemTable.COLUMN_NAME_QUESTIONID, question_id);
        values.put(OptionItemSchema.OptionItemTable.COLUMN_NAME_OPTIONID, option_id);
        values.put(OptionItemSchema.OptionItemTable.COLUMN_NAME_OPTION, newItem.getOptionAnswer());
        values.put(OptionItemSchema.OptionItemTable.COLUMN_NAME_OPTION_ORDER, newItem.getOrder());

        db.update(OptionItemSchema.OptionItemTable.TABLE_NAME, values, OptionItemSchema.OptionItemTable.COLUMN_NAME_SURVEYID +"='" + survey_id + "'"+" AND "+ OptionItemSchema.OptionItemTable.COLUMN_NAME_QUESTIONID +"='" +question_id +"'"+ " AND "+ OptionItemSchema.OptionItemTable.COLUMN_NAME_OPTIONID +"='" +option_id +"'", null);
        db.close();
    }
}
