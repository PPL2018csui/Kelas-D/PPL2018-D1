package com.example.windows81.kugen.activity;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.windows81.kugen.R;
import com.example.windows81.kugen.database.DatabaseHandler;
import com.example.windows81.kugen.database.OptionItemHandler;
import com.example.windows81.kugen.database.QuestionHandler;
import com.example.windows81.kugen.database.SubQuestionHandler;
import com.example.windows81.kugen.model.OptionItem;
import com.example.windows81.kugen.model.SurveyQuestion;
import com.example.windows81.kugen.task.AddResponseTask;
import com.example.windows81.kugen.task.GetQuestionTask;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Map;

public class QuestionActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";
    private DatabaseHandler dbHandler;
    private QuestionHandler questionHandler;
    private GetQuestionTask getQuestion;
    private View spinner;
    private LinearLayout layout_spinner;
    private String survey_id;
    private LinearLayout listView;

    private static JSONObject userAnswer;
    private static JSONObject userAssessment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        listView = (LinearLayout) findViewById(R.id.list_view222);
        userAnswer = new JSONObject();
        userAssessment = new JSONObject();
        dbHandler = new DatabaseHandler(this);
        survey_id = getIntent().getStringExtra("survey_id");
        questionHandler = new QuestionHandler(this);
        spinner = (ProgressBar)findViewById(R.id.progressBar1);
        layout_spinner = (LinearLayout) findViewById(R.id.layout_spinner);
        if (isConnected()) {
            spinner.setVisibility(View.VISIBLE);
            layout_spinner.setVisibility(View.VISIBLE);
            if(questionHandler.getAllQuestions(survey_id).isEmpty()){
                getQuestion = new GetQuestionTask(QuestionActivity.this, survey_id, false);
                getQuestion.execute();
            }else {
                getFromDb(survey_id);
                spinner.setVisibility(View.GONE);
                layout_spinner.setVisibility(View.GONE);
            }

        }else{
            getFromDb(survey_id);
        }
    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.refresh_menu, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_refresh) {
            Button submit = (Button) findViewById(R.id.submitSurvey);
            submit.setVisibility(View.VISIBLE);
            spinner.setVisibility(View.VISIBLE);
            layout_spinner.setVisibility(View.VISIBLE);
            if (isConnected()){
                QuestionHandler questionHandler = new QuestionHandler(QuestionActivity.this);
                questionHandler.deleteQuestionBasedOnSurveyId(survey_id);
                SubQuestionHandler subQuestionHandler = new SubQuestionHandler(QuestionActivity.this);
                subQuestionHandler.deleteSubQuestionBasedOnSurveyId(survey_id);
                OptionItemHandler optionItemHandler = new OptionItemHandler(QuestionActivity.this);
                optionItemHandler.deleteOptionBasedOnSurveyId(survey_id);
                this.listView.removeAllViews();
            }
            new GetQuestionTask(QuestionActivity.this,survey_id,false).execute();

        } else if (id == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    public JSONObject getUserAnswer(){
        return this.userAnswer;
    }
    public JSONObject getUserAnswerAssesment(){
        return this.userAssessment;
    }

    public void getFromDb(final String survey_id){
        ArrayList<SurveyQuestion> questionArrayList = questionHandler.getAllQuestions(survey_id);
        Button submit = (Button) findViewById(R.id.submitSurvey);
        LayoutInflater mInflater = (LayoutInflater) QuestionActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        LinearLayout mRecyclerView = (LinearLayout) findViewById(R.id.list_view222);
        for (int i=0; i < questionArrayList.size(); i++){
            LinearLayout layout;
            String type = questionArrayList.get(i).getQuestion_type();
            if (getItemViewType(i,questionArrayList) == 0) {
                Log.d("TEXT TYPE",i+"======"+type);
                layout = (LinearLayout) mInflater.inflate(R.layout.text_type, mRecyclerView, false);
                setText(layout, i, type, questionArrayList);
                final EditText inputText = (EditText) layout.findViewById(R.id.inputText);

                inputText.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                        try {
                            userAnswer.put("" + inputText.getTag(), "" + charSequence);

                            Log.d("TEXT CHANGE", inputText.getTag() + " " + charSequence);
                            Log.d("CURRENT USER ANSWER", "" + userAnswer.toString());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void afterTextChanged(Editable editable) {}
                });
            }else if (this.getItemViewType(i, questionArrayList) == 1) {
                Log.d("LIST TYPE",i+"===="+type);
                layout = (LinearLayout) mInflater.inflate(R.layout.radio_button_type, mRecyclerView, false);
                final RadioGroup viewGroup = (RadioGroup) layout.findViewById(R.id.radiogroup222);
                setText(layout, i, type, questionArrayList);

                viewGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                    public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
                        RadioButton checkedRadioButton = (RadioButton) radioGroup.findViewById(checkedId);
                        boolean isChecked = checkedRadioButton.isChecked();

                        if (isChecked) {
                            try{
                                userAnswer.put("" + viewGroup.getTag(), "" + checkedRadioButton.getText());
                                userAssessment.put("" + viewGroup.getTag(), "" + checkedRadioButton.getTag());
                                Log.d("RADIO CHECKED", viewGroup.getTag() + " " + checkedRadioButton.getText());
                                Log.d("CURRENT USER ANSWER", "" + userAnswer.toString());
                                Log.d("RADIO ASSESSMENT", ""  + checkedRadioButton.getTag());
                                Log.d("USER ASSESSMENT", "" + userAssessment.toString());
                            } catch (JSONException e){
                                e.printStackTrace();
                            }
                        }
                    }
                });

            } else if (this.getItemViewType(i, questionArrayList) == 2) {
                Log.d("MULTI TYPE",i+"===="+type);
                layout = (LinearLayout) mInflater.inflate(R.layout.check_box_type, mRecyclerView, false);
                setText(layout, i, type,questionArrayList);
            } else {
                layout = (LinearLayout) null;
                Log.d("ELSE TYPE",layout.toString());
            }
            mRecyclerView.addView(layout);
        }

        if (questionArrayList.isEmpty()){
            submit.setVisibility(View.GONE);
            Toast.makeText(QuestionActivity.this, "Anda tidak memiliki koneksi internet, dan anda belum mendownload pertanyaan ini untuk melakukannya secara offline", Toast.LENGTH_SHORT).show();
        } else {
            submit.setVisibility(View.VISIBLE);
        }

        submit.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ArrayList<JSONObject> arrayInput = new ArrayList<>();
                Log.d("aha", getUserAnswer().toString());
                arrayInput.add(getUserAnswer());
                arrayInput.add(getUserAnswerAssesment());
                Log.d("test", arrayInput.toString());
                AddResponseTask addResponse = new AddResponseTask(QuestionActivity.this,survey_id, arrayInput);

                final View spinner = (ProgressBar) findViewById(R.id.progress_bar);
                spinner.setVisibility(View.VISIBLE);
                addResponse.execute();
            }
        });
    }

    public boolean isConnected() {
        ConnectivityManager
                cm = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null
                && activeNetwork.isConnectedOrConnecting();
    }
    public int getItemViewType(int position, ArrayList<SurveyQuestion> menuList) {
        SurveyQuestion question = menuList.get(position);
        String questionType = question.getQuestion_type();
        if(question.getParent_id().equals("0")) {
            if (questionType.equalsIgnoreCase("T") || questionType.equalsIgnoreCase("S")) {
                return 0;
            } else if (questionType.equalsIgnoreCase("L")) {
                return 1;
            } else if (questionType.equalsIgnoreCase("M")) {
                return 2;
            } else {
                return -1;
            }
        } else {
            return -1;
        }

    }
    public void setText(LinearLayout layout, int position, String type, ArrayList<SurveyQuestion> menuList) {
        final SurveyQuestion objectQuestion = menuList.get(position);
        if (type.equals("T") || type.equals("S")) {
            String surveyId = objectQuestion.getSurvey_id();
            int groupId = objectQuestion.getGid();
            String questionId = objectQuestion.getQuestion_id();
            layout.setId(position);
            TextView title = (TextView) layout.findViewById(R.id.text);
            EditText input = (EditText) layout.findViewById(R.id.inputText);
            title.setText(objectQuestion.getQuestion());
            input.setTag(surveyId + "X" + groupId + "X" + questionId);
        } else if (type.equals("L")) {

            LinearLayout radioLayout = (LinearLayout) layout.findViewById(R.id.layout);
            RadioGroup radioGroup = (RadioGroup) layout.findViewById(R.id.radiogroup222);
            TextView title = (TextView) layout.findViewById(R.id.text);

            String sid, qid, oid;
            int gid, rgid, rbid;
            OptionItem optionItem;

            sid = objectQuestion.getSurvey_id();
            gid = objectQuestion.getGid();
            qid = objectQuestion.getQuestion_id();
            rgid = position;
            radioGroup.setId(Integer.parseInt(sid + "" + rgid));
            radioGroup.setTag(sid + "X" + gid + "X" + qid);
            radioLayout.setId(radioGroup.getId() + radioGroup.getId());
            title.setText(objectQuestion.getQuestion());

            int i = 0;
            for (final Map.Entry<String, OptionItem> entry : objectQuestion.getOptionItems().entrySet()) {
                RadioButton radioButton = new RadioButton(QuestionActivity.this);
                optionItem = entry.getValue();
                rbid = Integer.parseInt(radioGroup.getId() + "" + i++);
                radioButton.setId(rbid);
                radioButton.setText(optionItem.getOptionAnswer());
                radioButton.setTag(optionItem.getAssessmentValue());
                Log.d("ASSESSMENT", "" + optionItem.getAssessmentValue());
                radioGroup.addView(radioButton);
            }
        } else if (type.equals("M")) {
            LinearLayout checkboxList = (LinearLayout) layout.findViewById(R.id.checkboxList);
            TextView title = (TextView) layout.findViewById(R.id.text);

            String answer;
            String survey_id = objectQuestion.getSurvey_id();
            int group_id = objectQuestion.getGid();
            String question_id = objectQuestion.getQuestion_id();

            checkboxList.setId(position);

            title.setText(objectQuestion.getQuestion());

            for (Map.Entry<String, String> entry : objectQuestion.getSubQuestions().entrySet()) {
                CheckBox checkBox = new CheckBox(QuestionActivity.this);
                String oid = entry.getKey();
                answer = entry.getValue();
                checkBox.setTag(survey_id + "X" + group_id + "X" + question_id + oid);
                checkBox.setText(answer);
                checkboxList.addView(checkBox);

                checkBox.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        try {
                            if (((CheckBox) view).isChecked())
                                getUserAnswer().put(view.getTag() + "", ((CheckBox) view).getText().toString());
                            else
                                userAnswer.put(view.getTag() + "", "");
                            Log.d("CURRENT USER ANSWER", "" + userAnswer.toString());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        }
    }
}
