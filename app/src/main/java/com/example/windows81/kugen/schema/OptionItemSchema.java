package com.example.windows81.kugen.schema;

import android.provider.BaseColumns;

/**
 * Created by rifkiadrn on 4/21/2018.
 */

public class OptionItemSchema {
    private OptionItemSchema() {}

    public static class OptionItemTable implements BaseColumns {
        public static final String TABLE_NAME = "question_option_item";
        public static final String COLUMN_NAME_SURVEYID = "survey_id";
        public static final String COLUMN_NAME_QUESTIONID = "question_id";
        public static final String COLUMN_NAME_OPTIONID = "option_id";
        public static final String COLUMN_NAME_OPTION = "option";
        public static final String COLUMN_NAME_OPTION_ORDER = "option_order";
        public static final String COLUMN_NAME_ASSESSMENT_VALUE = "assessment_value";
    }
}
