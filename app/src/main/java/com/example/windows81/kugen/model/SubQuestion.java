package com.example.windows81.kugen.model;

import com.google.gson.annotations.SerializedName;

import java.util.Map;
import java.util.TreeMap;

/**
 * Created by rifkiadrn on 4/8/2018.
 */

public class SubQuestion {

    @SerializedName("available_answers")
    private Map<String,String> subQuestions;

    private boolean isSelected;

    public SubQuestion(TreeMap<String, String> subQuestions) {
        this.subQuestions = subQuestions;
        this.isSelected = false;
    }

    public Map<String, String> getSubQuestions() {
        return subQuestions;
    }

    public boolean isSelected() { return this.isSelected; }

    public void setSelected(boolean selected) {
        this.isSelected = selected;
    }
}
