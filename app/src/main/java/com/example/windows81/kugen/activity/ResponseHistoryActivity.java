package com.example.windows81.kugen.activity;

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.windows81.kugen.R;
import com.example.windows81.kugen.adapter.ResponseAdapter;
import com.example.windows81.kugen.database.SavedResponseHandler;
import com.example.windows81.kugen.task.AddResponseTask;
import com.example.windows81.kugen.task.GetQuestionTask;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by rifkiadrn on 4/23/2018.
 */

public class ResponseHistoryActivity extends DrawerActivity {
    private SavedResponseHandler savedResponseHandler;
    private Map<Integer,ArrayList<String>> savedResponse;
    private ResponseAdapter responseAdapter;
    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        NavigationView navigationView = findViewById(R.id.nav_view);
        MenuItem item = navigationView.getMenu().findItem(R.id.nav_upload);
        item.setChecked(true);

        FrameLayout contentFrameLayout = (FrameLayout) findViewById(R.id.content_bookmark); //Remember this is the FrameLayout area within your activity_main.xml
        getLayoutInflater().inflate(R.layout.activity_responsehistory, contentFrameLayout);
        setTitle("Riwayat Response Gagal");

        this.savedResponseHandler = new SavedResponseHandler(ResponseHistoryActivity.this);
        this.savedResponse = savedResponseHandler.getAllResponse();

        this.responseAdapter = new ResponseAdapter(ResponseHistoryActivity.this, R.layout.responsehistory_item, savedResponse);

        ListView view = (ListView) findViewById(R.id.listview_response);
        view.setAdapter(this.responseAdapter);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.submit_menu, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_submit) {
            JSONObject objResponse = null;
            View spinner = (ProgressBar) findViewById(R.id.progress_bar);
            if(this.savedResponse.isEmpty())
                Toast.makeText(ResponseHistoryActivity.this, "Tidak ada response yang belum tersubmit", Toast.LENGTH_SHORT).show();
            else{
                for (Integer key: this.savedResponse.keySet()){
                    try {
                        objResponse = new JSONObject(this.savedResponse.get(key).get(1));
                    } catch (JSONException e){
                        e.printStackTrace();
                    }
                    ArrayList<JSONObject> arr = new ArrayList<>();
                    arr.add(objResponse);
                    spinner.setVisibility(View.VISIBLE);
                    new AddResponseTask(ResponseHistoryActivity.this,this.savedResponse.get(key).get(0),arr).execute(key);
                }
            }
        } else if (id == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

}
