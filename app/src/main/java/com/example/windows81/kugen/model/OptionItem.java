package com.example.windows81.kugen.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by rifkiadrn on 3/29/2018.
 */

public class OptionItem {

    @SerializedName("answer")
    private String optionAnswer;

    @SerializedName("order")
    private int order;

    @SerializedName("assessment_value")
    private int assessmentValue;

    private boolean isSelected;

    public  OptionItem(){
        this.optionAnswer= "";
        this.order = 0;
        this.isSelected = false;
    }
    public OptionItem(String optionAnswer, int order) {
        this.optionAnswer = optionAnswer;
        this.order = order;
        this.isSelected = false;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public boolean isSelected()  { return this.isSelected; }

    public void setSelected(boolean selected) { this.isSelected = selected; }

    public String getOptionAnswer() {
        return optionAnswer;
    }

    public void setOptionAnswer(String optionAnswer) {
        this.optionAnswer = optionAnswer;
    }

    public int getOrder() {
        return order;
    }

    public int getAssessmentValue() {
        return assessmentValue;
    }

    public void setAssessmentValue(int assessmentValue) {
        this.assessmentValue = assessmentValue;
    }
}
