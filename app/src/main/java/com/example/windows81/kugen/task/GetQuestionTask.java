package com.example.windows81.kugen.task;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.windows81.kugen.R;
import com.example.windows81.kugen.activity.QuestionActivity;
import com.example.windows81.kugen.database.QuestionHandler;
import com.example.windows81.kugen.model.OptionItem;
import com.example.windows81.kugen.model.OptionsResult;
import com.example.windows81.kugen.model.SubQuestion;
import com.example.windows81.kugen.model.SurveyQuestion;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;
import java.util.TreeMap;


/**
 * Created by Windows 8.1 on 3/20/2018.
 */

public class GetQuestionTask extends AsyncTask<Object, Object, ArrayList<SurveyQuestion>> {

    private Context context;
    private String surveyId;
    private boolean isDownload;
    private String error;
    private JSONObject userAnswer;
    private JSONObject userAssessment;

    public GetQuestionTask(Context context, String surveyId, boolean isDownload) {
        this.isDownload = isDownload;
        this.context = context;
        this.surveyId = surveyId;
        this.userAnswer = new JSONObject();
        this.userAssessment = new JSONObject();
    }

    @Override
    public ArrayList<SurveyQuestion> doInBackground(Object... params) {
        QuestionHandler qhandler = new QuestionHandler(context);
        DefaultHttpClient client = new DefaultHttpClient();
        ArrayList<SurveyQuestion> questionList = new ArrayList<>();
        ArrayList<SurveyQuestion> removedSub = new ArrayList<>();
        Log.d("getquestiontask","doin");
        try {
            Log.d("aodkao","aokdoa");
            HttpPost post = new HttpPost("https://kuesionergenerik.limequery.com/admin/remotecontrol");
            Log.d("aodkao2",post.toString());
            post.setHeader("Content-type", "application/json");
            post.setEntity(new StringEntity("{\"method\": \"get_session_key\", \"params\": [\"KugenPPL\", \"kugenasoy\"], \"id\": 1}"));
            Log.d("masuksini","aodkao");
            try {
                HttpResponse response = client.execute(post);
                Log.d("response", response.toString());
                if (response.getStatusLine().getStatusCode() == 200) {
                    HttpEntity entity = response.getEntity();
                    String sessionKey = parse(EntityUtils.toString(entity));
                    Log.d("sessionkey", sessionKey);
                    post.setEntity( new StringEntity("{\"method\": \"list_questions\", \"params\": [ "+sessionKey+", "+surveyId+" ], \"id\": 1}"));
                    response = client.execute(post);
                    if (response.getStatusLine().getStatusCode() == 200) {
                        entity = response.getEntity();
                        JsonElement jsonRes = new JsonParser().parse(EntityUtils.toString(entity));
                        Gson gson = new Gson();
                        questionList = gson.fromJson(jsonRes.getAsJsonObject().get("result").toString(), new TypeToken<ArrayList<SurveyQuestion>>(){}.getType());
                        for (int i=0; i < questionList.size(); i++){
                            post.setEntity( new StringEntity("{\"method\": \"get_question_properties\", \"params\": [ "+sessionKey+", "+questionList.get(i).getQuestion_id()+" ], \"id\": 1}"));
                            response = client.execute(post);
                            entity = response.getEntity();
                            JsonElement temp = new JsonParser().parse(EntityUtils.toString(entity));
                            Log.d("getquestionres",temp.toString());
                            OptionsResult answerOption;
                            SubQuestion subQuestion;
                            if (questionList.get(i).getParent_id().equals("0")){
                                try {
                                    answerOption = gson.fromJson(temp.getAsJsonObject().get("result").toString(), new TypeToken<OptionsResult>() {
                                    }.getType());
                                    Log.d("options-size", "" + answerOption.getAnswerOptions().size());
                                    questionList.get(i).setOptionItems(answerOption.getAnswerOptions());
//                                questionList.get(i).setOptionItems(opt);
                                    Log.d("Options of Questions", "========> "+questionList.get(i).getOptionItems().keySet());
                                }catch (Exception e){
                                    questionList.get(i).setOptionItems(new TreeMap<String, OptionItem>());
                                }
                                try{
                                    subQuestion = gson.fromJson(temp.getAsJsonObject().get("result").toString(), new TypeToken<SubQuestion>() {
                                    }.getType());
                                    questionList.get(i).setSubQuestions(subQuestion.getSubQuestions());
                                    Log.d("Options type 2", "========> "+questionList.get(i).getSubQuestions().keySet());
                                } catch (Exception e){
                                    questionList.get(i).setSubQuestions(new TreeMap<String, String>());
                                }
                                removedSub.add(questionList.get(i));
                            }
                        }
                        qhandler.addAllQuestions(removedSub);
                        Log.d("get Questions", jsonRes.getAsJsonObject().get("result").toString());

                    }else{
                        Log.d("get surveys error", EntityUtils.toString(entity));
                    }
                    return removedSub;
                }
            } catch (UnknownHostException e){
                error = "Gagal untuk mendownload pertanyaan, anda tidak memiliki koneksi internet.";
                Log.d("koko",e.getMessage());
            } catch (IOException e) {
                error = "Error dalam mendapatkan data";
                Log.d("koko2", e.getMessage());
            } catch (Exception e){
                Log.d("koko3", e.getMessage());
                e.printStackTrace();
            }
        } catch (UnsupportedEncodingException e){
            error = "error dalam mendapatkan data";
            e.printStackTrace();
        } catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void onPostExecute(ArrayList questionItems) {
        final Activity mainAct = (Activity) context;
        ArrayList<SurveyQuestion> questionArrayList = questionItems;

        if (questionItems != null && !this.isDownload) {
            Collections.sort(questionArrayList);
            final LinearLayout mRecyclerView = (LinearLayout) mainAct.findViewById(R.id.list_view222);

            LayoutInflater mInflater = (LayoutInflater) mainAct.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            for (int i=0; i < questionArrayList.size(); i++){
                LinearLayout layout;
                String type = questionArrayList.get(i).getQuestion_type();
                if (getItemViewType(i,questionArrayList) == 0) {
                    Log.d("TEXT TYPE",i+"======"+type);
                    layout = (LinearLayout) mInflater.inflate(R.layout.text_type, mRecyclerView, false);
                    setText(layout, i, type, questionArrayList);
                    final EditText inputText = (EditText) layout.findViewById(R.id.inputText);

                    inputText.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

                        @Override
                        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                            try {
                                userAnswer.put("" + inputText.getTag(), "" + charSequence);
                                Log.d("TEXT CHANGE", inputText.getTag() + " " + charSequence);
                                Log.d("CURRENT USER ANSWER", "" + userAnswer.toString());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        @Override
                        public void afterTextChanged(Editable editable) {}
                    });
                }else if (this.getItemViewType(i, questionArrayList) == 1) {
                    Log.d("LIST TYPE",i+"===="+type);
                    layout = (LinearLayout) mInflater.inflate(R.layout.radio_button_type, mRecyclerView, false);
                    final RadioGroup viewGroup = (RadioGroup) layout.findViewById(R.id.radiogroup222);
                    setText(layout, i, type, questionArrayList);

                    viewGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                        public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
                            RadioButton checkedRadioButton = (RadioButton) radioGroup.findViewById(checkedId);
                            boolean isChecked = checkedRadioButton.isChecked();

                            if (isChecked) {
                                try{
                                    userAnswer.put("" + viewGroup.getTag(), "" + checkedRadioButton.getText());
                                    userAssessment.put("" + viewGroup.getTag(), "" + checkedRadioButton.getTag());
                                    Log.d("RADIO CHECKED", viewGroup.getTag() + " " + checkedRadioButton.getText());
                                    Log.d("CURRENT USER ANSWER", "" + userAnswer.toString());
                                    Log.d("RADIO ASSESSMENT", "" + checkedRadioButton.getTag());
                                    Log.d("USER ASSESSMENT", "" + userAssessment.toString());
                                } catch (JSONException e){
                                    e.printStackTrace();
                                }
                            }
                        }
                    });

                } else if (this.getItemViewType(i, questionArrayList) == 2) {
                    Log.d("MULTI TYPE",i+"===="+type);
                    layout = (LinearLayout) mInflater.inflate(R.layout.check_box_type, mRecyclerView, false);
                    setText(layout, i, type,questionArrayList);
                } else {
                    layout = (LinearLayout) null;
                    Log.d("ELSE TYPE",layout.toString());
                }
                mRecyclerView.addView(layout);
            }

            final View spinner = (ProgressBar) mainAct.findViewById(R.id.progressBar1);
            final LinearLayout layout_spinner = (LinearLayout) mainAct.findViewById(R.id.layout_spinner);
            spinner.setVisibility(View.GONE);
            layout_spinner.setVisibility(View.GONE);
            Toast.makeText(context,"retrieve success",Toast.LENGTH_SHORT).show();

            Button submit = (Button) mainAct.findViewById(R.id.submitSurvey);

            submit.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    ArrayList<JSONObject> arrayInput = new ArrayList<>();
                    Log.d("aha", userAnswer.toString());
                    arrayInput.add(userAnswer);
                    arrayInput.add(userAssessment);
                    Log.d("test", arrayInput.toString());
                    AddResponseTask addResponse = new AddResponseTask(mainAct,surveyId, arrayInput);

                    final View spinner = (ProgressBar) mainAct.findViewById(R.id.progress_bar);
                    spinner.setVisibility(View.VISIBLE);
                    addResponse.execute();
                }
            });
        } else if(questionItems != null && isDownload){
            QuestionHandler questionHandler = new QuestionHandler(context);
            questionHandler.addAllQuestions(questionItems);
            final View spinner = (ProgressBar) mainAct.findViewById(R.id.progress_bar2);
            spinner.setVisibility(View.GONE);
            Toast.makeText(context,"Daftar pertanyaan sudah terdownload, anda bisa melakukan survey kapan pun dan dimana pun!", Toast.LENGTH_SHORT).show();
        }
        if(questionItems == null){
            if (isDownload){
                final View spinner = (ProgressBar) mainAct.findViewById(R.id.progress_bar2);
                spinner.setVisibility(View.GONE);
                Toast.makeText(context, "Download:  "+error, Toast.LENGTH_SHORT).show();
            }else{
                final View spinner = (ProgressBar) mainAct.findViewById(R.id.progressBar1);
                LinearLayout layout_spinner = (LinearLayout) mainAct.findViewById(R.id.layout_spinner);
                spinner.setVisibility(View.GONE);
                layout_spinner.setVisibility(View.GONE);
                Button btnSubmit = (Button) mainAct.findViewById(R.id.submitSurvey);
                LinearLayout view = (LinearLayout) mainAct.findViewById(R.id.list_view222);
                if (view.getChildCount() == 0)
                    btnSubmit.setVisibility(View.GONE);
                Toast.makeText(context, "Not download: "+error, Toast.LENGTH_SHORT).show();
            }

        }
    }

    public static String parse(String jsonLine) {
        JsonElement jelement = new JsonParser().parse(jsonLine);
        Log.d("jelement", jelement.toString());
        JsonObject jobject = jelement.getAsJsonObject();
        String result = jobject.get("result").toString();
        return result;
    }


    public int getItemViewType(int position, ArrayList<SurveyQuestion> menuList) {
        SurveyQuestion question = menuList.get(position);
        String questionType = question.getQuestion_type();
        if(question.getParent_id().equals("0")) {
            if (questionType.equalsIgnoreCase("T") || questionType.equalsIgnoreCase("S")) {
                return 0;
            } else if (questionType.equalsIgnoreCase("L")) {
                return 1;
            } else if (questionType.equalsIgnoreCase("M")) {
                return 2;
            } else {
                return -1;
            }
        } else {
            return -1;
        }

    }


    public void setText(LinearLayout layout, int position, String type, ArrayList<SurveyQuestion> menuList) {
        final SurveyQuestion objectQuestion = menuList.get(position);
        if (type.equals("T") || type.equals("S")) {
            String surveyId = objectQuestion.getSurvey_id();
            int groupId = objectQuestion.getGid();
            String questionId = objectQuestion.getQuestion_id();
            layout.setId(position);
            TextView title = (TextView) layout.findViewById(R.id.text);
            EditText input = (EditText) layout.findViewById(R.id.inputText);
            title.setText(objectQuestion.getQuestion());
            input.setTag(surveyId + "X" + groupId + "X" + questionId);
        } else if (type.equals("L")) {

            LinearLayout radioLayout = (LinearLayout) layout.findViewById(R.id.layout);
            RadioGroup radioGroup = (RadioGroup) layout.findViewById(R.id.radiogroup222);
            TextView title = (TextView) layout.findViewById(R.id.text);

            String sid, qid, oid;
            int gid, rgid, rbid;
            OptionItem optionItem;

            sid = objectQuestion.getSurvey_id();
            gid = objectQuestion.getGid();
            qid = objectQuestion.getQuestion_id();
            rgid = position;
            radioGroup.setId(Integer.parseInt(sid + "" + rgid));
            radioGroup.setTag(sid + "X" + gid + "X" + qid);
            radioLayout.setId(radioGroup.getId() + radioGroup.getId());
            title.setText(objectQuestion.getQuestion());

            int i = 0;
            for (final Map.Entry<String, OptionItem> entry : objectQuestion.getOptionItems().entrySet()) {
                RadioButton radioButton = new RadioButton(context);
                optionItem = entry.getValue();
                rbid = Integer.parseInt(radioGroup.getId() + "" + i++);
                radioButton.setId(rbid);
                radioButton.setText(optionItem.getOptionAnswer());
                radioButton.setTag(optionItem.getAssessmentValue());
                Log.d("ASSESSMENT", "" + optionItem.getAssessmentValue());
                radioGroup.addView(radioButton);
            }
        } else if (type.equals("M")) {
            LinearLayout checkboxList = (LinearLayout) layout.findViewById(R.id.checkboxList);
            TextView title = (TextView) layout.findViewById(R.id.text);

            String answer;
            String survey_id = objectQuestion.getSurvey_id();
            int group_id = objectQuestion.getGid();
            String question_id = objectQuestion.getQuestion_id();

            checkboxList.setId(position);

            title.setText(objectQuestion.getQuestion());

            for (Map.Entry<String, String> entry : objectQuestion.getSubQuestions().entrySet()) {
                CheckBox checkBox = new CheckBox(context);
                String oid = entry.getKey();
                answer = entry.getValue();
                checkBox.setTag(survey_id + "X" + group_id + "X" + question_id + oid);
                checkBox.setText(answer);
                checkboxList.addView(checkBox);

                checkBox.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        try {
                            if (((CheckBox) view).isChecked())
                                userAnswer.put(view.getTag() + "", ((CheckBox) view).getText().toString());
                            else
                                userAnswer.put(view.getTag() + "", "");
                            Log.d("CURRENT USER ANSWER", "" + userAnswer.toString());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        }
    }
}