package com.example.windows81.kugen.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;

import com.example.windows81.kugen.R;
import com.example.windows81.kugen.activity.MainActivity;
import com.example.windows81.kugen.activity.DetailActivity;

public class DrawerActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    public DrawerLayout drawer;
    public Toolbar toolbar;
    private ActionBarDrawerToggle toggle;
    private NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drawer);

        navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        toolbar =(Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_menu);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem item) {
//                item.setChecked(true);
                navigationView.setCheckedItem(item.getItemId());
                Log.d("CEK ITEM", "" + item.isChecked());
                switchId(item.getItemId());
                return false;
            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    public void setDrawer(int id, Context start, Class destination) {
        Intent intent = new Intent(start, destination);
        startActivity(intent);
        drawer.closeDrawers();
    }

    public void switchId(int id){
        switch (id){
            case R.id.nav_dashboard:
                setDrawer(id, getApplicationContext(), MainActivity.class);
                break;
            case R.id.nav_upload:
                setDrawer(id, getApplicationContext(), ResponseHistoryActivity.class);
                break;
            default:
                setDrawer(id, getApplicationContext(), DetailActivity.class);
                break;
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
//        int id = item.getItemId();
//
//        if (id == R.id.nav_dashboard) {
//            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
//            startActivity(intent);
//            drawer.closeDrawers();
//        } else if (id == R.id.nav_profile) {
//            Intent intent = new Intent(getApplicationContext(), DetailActivity.class);
//            Log.d("masuk sini","masuk");
//            startActivity(intent);
//            drawer.closeDrawers();
//        } else if (id == R.id.nav_my_survey) {
//            Intent intent = new Intent(getApplicationContext(), DetailActivity.class);
//            startActivity(intent);
//            drawer.closeDrawers();
//        } else if (id == R.id.nav_upload) {
//            Intent intent = new Intent(getApplicationContext(), DetailActivity.class);
//            startActivity(intent);
//            drawer.closeDrawers();
//        } else if (id == R.id.nav_setting) {
//            Intent intent = new Intent(getApplicationContext(), DetailActivity.class);
//            startActivity(intent);
//            drawer.closeDrawers();
//        }
//
//        DrawerLayout drawer = findViewById(R.id.drawer_layout);
//        drawer.closeDrawer(GravityCompat.START);
//        return true;
        return true;
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState){
        super.onPostCreate(savedInstanceState);
        toggle.syncState();
    }
}
