package com.example.windows81.kugen.schema;

import android.provider.BaseColumns;

/**
 * Created by rifkiadrn on 3/11/2018.
 */

public class SurveyQuestionsSchema {
    private SurveyQuestionsSchema() {}

    public static class SurveyQuestionsTable implements BaseColumns {
        public static final String TABLE_NAME = "survey_questions";
        public static final String COLUMN_NAME_QUESTIONID = "question_id";
        public static final String COLUMN_NAME_PARENTID = "parent_id";
        public static final String COLUMN_NAME_SURVEYID = "survey_id";
        public static final String COLUMN_NAME_QUESTIONTYPE = "question_type";
        public static final String COLUMN_NAME_TITLE = "title";
        public static final String COLUMN_NAME_QUESTION = "question";
        public static final String COLUMN_NAME_QUESTIONORDER = "question_order";
        public static final String COLUMN_NAME_GID = "gid";
    }

}