package com.example.windows81.kugen.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.windows81.kugen.model.SurveyQuestion;
import com.example.windows81.kugen.schema.SubQuestionSchema;

import java.util.Map;
import java.util.TreeMap;

/**
 * Created by rifkiadrn on 4/21/2018.
 */

public class SubQuestionHandler extends DatabaseHandler {
    private SQLiteDatabase db;

    public SubQuestionHandler(Context context) {
        super(context);
        this.db = super.getDatabase();
    }

    public void addSubQuestion(String survey_id, String question_id, String subquestion_id, String subQuestion){
        db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(SubQuestionSchema.SubQuestionTable.COLUMN_NAME_QUESTIONID, question_id);
        values.put(SubQuestionSchema.SubQuestionTable.COLUMN_NAME_SURVEYID, survey_id);
        values.put(SubQuestionSchema.SubQuestionTable.COLUMN_NAME_SUBQUESTIONID, subquestion_id);
        values.put(SubQuestionSchema.SubQuestionTable.COLUMN_NAME_SUBQUESTION, subQuestion);

        db.insert(SubQuestionSchema.SubQuestionTable.TABLE_NAME, null, values);
    }
    public void addAllSubQuestions(SurveyQuestion sq) {
        for (String subQuestion_id: sq.getSubQuestions().keySet()){
            if(!isContains(sq.getSurvey_id(), sq.getQuestion_id(), subQuestion_id)){
                addSubQuestion(sq.getSurvey_id(), sq.getQuestion_id(), subQuestion_id, sq.getSubQuestions().get(subQuestion_id));
            } else {
                updateSubQuestion(sq.getSurvey_id(), sq.getQuestion_id(), subQuestion_id, sq.getSubQuestions().get(subQuestion_id));
            }
        }
    }

    public Map<String, String> getAllSubQuestions(SurveyQuestion sq) {
        db = getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT * FROM " + SubQuestionSchema.SubQuestionTable.TABLE_NAME +" WHERE "+ SubQuestionSchema.SubQuestionTable.COLUMN_NAME_SURVEYID +"='" + sq.getSurvey_id() + "'"+" AND "+ SubQuestionSchema.SubQuestionTable.COLUMN_NAME_QUESTIONID +"='" +sq.getQuestion_id() +"'"+ " ORDER BY " + SubQuestionSchema.SubQuestionTable.COLUMN_NAME_SUBQUESTIONID + " ASC", null);

        Map<String, String> subQuestionList = new TreeMap<>();

        // Traversing through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                String subQuestion_id = cursor.getString(cursor.getColumnIndex(SubQuestionSchema.SubQuestionTable.COLUMN_NAME_SUBQUESTIONID));
                String subQuestion = cursor.getString(cursor.getColumnIndex(SubQuestionSchema.SubQuestionTable.COLUMN_NAME_SUBQUESTION));

                subQuestionList.put(subQuestion_id, subQuestion);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return subQuestionList;
    }

    public String getSubQuestion(String survey_id, String question_id, String subquestion_id) {
        db = getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT * FROM " + SubQuestionSchema.SubQuestionTable.TABLE_NAME +" WHERE "+ SubQuestionSchema.SubQuestionTable.COLUMN_NAME_SURVEYID +"='" + survey_id + "'"+" AND "+ SubQuestionSchema.SubQuestionTable.COLUMN_NAME_QUESTIONID +"='" +question_id +"'" +" AND "+ SubQuestionSchema.SubQuestionTable.COLUMN_NAME_SUBQUESTIONID +"='" +subquestion_id +"'"+ " ORDER BY " + SubQuestionSchema.SubQuestionTable.COLUMN_NAME_SUBQUESTIONID + " ASC", null);
        String subQuestion = "";
        if (cursor.moveToFirst()){
            subQuestion = cursor.getString(cursor.getColumnIndex(SubQuestionSchema.SubQuestionTable.COLUMN_NAME_SUBQUESTION));
        }
        cursor.close();
        db.close();
        return subQuestion;
    }

    public boolean isContains(String survey_id, String question_id, String subquestion_id) {
        db = getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT * FROM " + SubQuestionSchema.SubQuestionTable.TABLE_NAME +" WHERE "+ SubQuestionSchema.SubQuestionTable.COLUMN_NAME_SURVEYID +"='" + survey_id + "'"+" AND "+ SubQuestionSchema.SubQuestionTable.COLUMN_NAME_QUESTIONID +"='" +question_id +"'"+ " AND "+ SubQuestionSchema.SubQuestionTable.COLUMN_NAME_SUBQUESTIONID +"='" +subquestion_id +"'", null);
        if (cursor.getCount() > 0) {
            return true;
        }
        return false;
    }

    public void deleteSubQuestion(String survey_id, String question_id, String subquestion_id) {
        db = getWritableDatabase();
        if (isContains(survey_id,question_id,subquestion_id)) {
            db.delete(SubQuestionSchema.SubQuestionTable.TABLE_NAME, SubQuestionSchema.SubQuestionTable.COLUMN_NAME_SURVEYID +"='" + survey_id + "'"+" AND "+ SubQuestionSchema.SubQuestionTable.COLUMN_NAME_QUESTIONID +"='" +question_id +"'"+ " AND "+ SubQuestionSchema.SubQuestionTable.COLUMN_NAME_SUBQUESTIONID +"='" +subquestion_id +"'", null);
            db.close();
        }
    }

    public void deleteSubQuestionBasedOnSurveyId(String survey_id){
        db = getWritableDatabase();
        db.delete(SubQuestionSchema.SubQuestionTable.TABLE_NAME, SubQuestionSchema.SubQuestionTable.COLUMN_NAME_SURVEYID +"='"+survey_id+"'", null);
        db.close();
    }

    public void deleteAllSubQuestions(SurveyQuestion sq) {
        db = getWritableDatabase();
        for (String subQuestion_id: sq.getSubQuestions().keySet()){
            if (isContains(sq.getSurvey_id(), sq.getQuestion_id(), subQuestion_id)){
                deleteSubQuestion(sq.getSurvey_id(), sq.getQuestion_id(), subQuestion_id);
            }
        }
    }

    public void updateSubQuestion(String survey_id, String question_id, String subquestion_id, String newSubQuestion){
        db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(SubQuestionSchema.SubQuestionTable.COLUMN_NAME_SURVEYID, survey_id);
        values.put(SubQuestionSchema.SubQuestionTable.COLUMN_NAME_QUESTIONID, question_id);
        values.put(SubQuestionSchema.SubQuestionTable.COLUMN_NAME_SUBQUESTIONID, subquestion_id);
        values.put(SubQuestionSchema.SubQuestionTable.COLUMN_NAME_SUBQUESTION, newSubQuestion);

        db.update(SubQuestionSchema.SubQuestionTable.TABLE_NAME, values, SubQuestionSchema.SubQuestionTable.COLUMN_NAME_SURVEYID +"='" + survey_id + "'"+" AND "+ SubQuestionSchema.SubQuestionTable.COLUMN_NAME_QUESTIONID +"='" +question_id +"'"+ " AND "+ SubQuestionSchema.SubQuestionTable.COLUMN_NAME_SUBQUESTIONID +"='" +subquestion_id +"'", null);
        db.close();
    }
}
