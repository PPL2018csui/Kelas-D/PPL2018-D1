package com.example.windows81.kugen.activity;

        import android.support.design.widget.NavigationView;
        import android.support.design.widget.TabLayout;
        import android.support.v4.view.ViewPager;
        import android.os.Bundle;
        import android.view.MenuItem;
        import android.widget.FrameLayout;

        import com.example.windows81.kugen.R;
        import com.example.windows81.kugen.adapter.PagerAdapter;
        import com.example.windows81.kugen.database.DatabaseHandler;

public class MainActivity extends DrawerActivity {
    private static final String TAG = "MainActivity";
    private FrameLayout contentFrameLayout;
    private ViewPager viewPager;
    private PagerAdapter adapter;
    private DatabaseHandler dbHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dbHandler = new DatabaseHandler(MainActivity.this);
        NavigationView navigationView = findViewById(R.id.nav_view);
        MenuItem item = navigationView.getMenu().findItem(R.id.nav_dashboard);
        item.setChecked(true);

        contentFrameLayout = findViewById(R.id.content_bookmark); //Remember this is the FrameLayout area within your activity_main.xml
        getLayoutInflater().inflate(R.layout.activity_main, contentFrameLayout);
        setTitle(getResources().getString(R.string.menu_dashboard));

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText("Etalase"));
        tabLayout.addTab(tabLayout.newTab().setText("riwayat"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        viewPager = (ViewPager) findViewById(R.id.pager);
        adapter = new PagerAdapter(getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

}
