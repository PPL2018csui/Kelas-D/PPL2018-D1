package com.example.windows81.kugen.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.windows81.kugen.model.SubQuestion;
import com.example.windows81.kugen.model.SurveyQuestion;
import com.example.windows81.kugen.schema.SurveyQuestionsSchema;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rifkiadrn on 3/26/2018.
 */
public class QuestionHandler extends DatabaseHandler {

    private SQLiteDatabase db;
    private OptionItemHandler optionItemHandler;
    private SubQuestionHandler subQuestionHandler;

    public QuestionHandler(Context context) {
        super(context);
        this.db = super.getDatabase();
        this.optionItemHandler = new OptionItemHandler(context);
        this.subQuestionHandler = new SubQuestionHandler(context);
    }

    //CREATE
    public void addQuestion(SurveyQuestion question) {
        db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(SurveyQuestionsSchema.SurveyQuestionsTable.COLUMN_NAME_QUESTIONID, question.getQuestion_id());
        values.put(SurveyQuestionsSchema.SurveyQuestionsTable.COLUMN_NAME_PARENTID, question.getParent_id());
        values.put(SurveyQuestionsSchema.SurveyQuestionsTable.COLUMN_NAME_SURVEYID, question.getSurvey_id());
        values.put(SurveyQuestionsSchema.SurveyQuestionsTable.COLUMN_NAME_QUESTIONTYPE, question.getQuestion_type());
        values.put(SurveyQuestionsSchema.SurveyQuestionsTable.COLUMN_NAME_TITLE, question.getTitle());
        values.put(SurveyQuestionsSchema.SurveyQuestionsTable.COLUMN_NAME_QUESTION, question.getQuestion());
        values.put(SurveyQuestionsSchema.SurveyQuestionsTable.COLUMN_NAME_QUESTIONORDER, question.getQuestion_order());
        values.put(SurveyQuestionsSchema.SurveyQuestionsTable.COLUMN_NAME_GID, question.getGid());

        db.insert(SurveyQuestionsSchema.SurveyQuestionsTable.TABLE_NAME, null, values);

    }

    public void addAllQuestions(List<SurveyQuestion> questionItems) {
        for (SurveyQuestion questionItem : questionItems) {
            if (!isContains(questionItem.getQuestion_id())) {
                addQuestion(questionItem);
            } else {
                updateQuestion(questionItem);
            }
            if (questionItem.getOptionItems() != null && !questionItem.getOptionItems().isEmpty())
                optionItemHandler.addAllOption(questionItem);
            if (questionItem.getSubQuestions() != null && !questionItem.getSubQuestions().isEmpty())
                subQuestionHandler.addAllSubQuestions(questionItem);
        }
    }

    //RETRIEVE
    public ArrayList<SurveyQuestion> getAllQuestions(String survey_id) {
        db = getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT * FROM " + SurveyQuestionsSchema.SurveyQuestionsTable.TABLE_NAME +" WHERE "+ SurveyQuestionsSchema.SurveyQuestionsTable.COLUMN_NAME_SURVEYID +"='" + survey_id + "'" + " ORDER BY " + SurveyQuestionsSchema.SurveyQuestionsTable.COLUMN_NAME_GID + " ASC,"+ SurveyQuestionsSchema.SurveyQuestionsTable.COLUMN_NAME_QUESTIONORDER + " ASC", null);

        ArrayList<SurveyQuestion> questionList = new ArrayList<SurveyQuestion>();

        // Traversing through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                SurveyQuestion question = new SurveyQuestion();
                question.setQuestion_id(cursor.getString(cursor.getColumnIndex(SurveyQuestionsSchema.SurveyQuestionsTable.COLUMN_NAME_QUESTIONID)));
                question.setParent_id(cursor.getString(cursor.getColumnIndex(SurveyQuestionsSchema.SurveyQuestionsTable.COLUMN_NAME_PARENTID)));
                question.setSurvey_id(cursor.getString(cursor.getColumnIndex(SurveyQuestionsSchema.SurveyQuestionsTable.COLUMN_NAME_SURVEYID)));
                question.setQuestion_type(cursor.getString(cursor.getColumnIndex(SurveyQuestionsSchema.SurveyQuestionsTable.COLUMN_NAME_QUESTIONTYPE)));
                question.setTitle(cursor.getString(cursor.getColumnIndex(SurveyQuestionsSchema.SurveyQuestionsTable.COLUMN_NAME_TITLE)));
                question.setQuestion(cursor.getString(cursor.getColumnIndex(SurveyQuestionsSchema.SurveyQuestionsTable.COLUMN_NAME_QUESTION)));
                question.setQuestion_order(Integer.parseInt(cursor.getString(cursor.getColumnIndex(SurveyQuestionsSchema.SurveyQuestionsTable.COLUMN_NAME_QUESTIONORDER))));
                question.setGid(Integer.parseInt(cursor.getString(cursor.getColumnIndex(SurveyQuestionsSchema.SurveyQuestionsTable.COLUMN_NAME_GID))));
                question.setOptionItems(optionItemHandler.getAllOption(question));
                question.setSubQuestions(subQuestionHandler.getAllSubQuestions(question));
                questionList.add(question);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        // return question list
        return questionList;

    }

    public SurveyQuestion getQuestion(String question_id) {
        db = getReadableDatabase();
        SurveyQuestion question = null;

        Cursor cursor = db.rawQuery("SELECT * FROM " + SurveyQuestionsSchema.SurveyQuestionsTable.TABLE_NAME + " where " + SurveyQuestionsSchema.SurveyQuestionsTable.COLUMN_NAME_QUESTIONID + "='" + question_id + "'", null);

        // Traversing through all rows and adding to list
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            question = new SurveyQuestion();
            question.setQuestion_id(cursor.getString(cursor.getColumnIndex(SurveyQuestionsSchema.SurveyQuestionsTable.COLUMN_NAME_QUESTIONID)));
            question.setParent_id(cursor.getString(cursor.getColumnIndex(SurveyQuestionsSchema.SurveyQuestionsTable.COLUMN_NAME_PARENTID)));
            question.setSurvey_id(cursor.getString(cursor.getColumnIndex(SurveyQuestionsSchema.SurveyQuestionsTable.COLUMN_NAME_SURVEYID)));
            question.setQuestion_type(cursor.getString(cursor.getColumnIndex(SurveyQuestionsSchema.SurveyQuestionsTable.COLUMN_NAME_QUESTIONTYPE)));
            question.setTitle(cursor.getString(cursor.getColumnIndex(SurveyQuestionsSchema.SurveyQuestionsTable.COLUMN_NAME_TITLE)));
            question.setQuestion(cursor.getString(cursor.getColumnIndex(SurveyQuestionsSchema.SurveyQuestionsTable.COLUMN_NAME_QUESTION)));
            question.setQuestion_order(Integer.parseInt(cursor.getString(cursor.getColumnIndex(SurveyQuestionsSchema.SurveyQuestionsTable.COLUMN_NAME_QUESTIONORDER))));
            question.setGid(Integer.parseInt(cursor.getString(cursor.getColumnIndex(SurveyQuestionsSchema.SurveyQuestionsTable.COLUMN_NAME_GID))));
            question.setOptionItems(optionItemHandler.getAllOption(question));
            question.setSubQuestions(subQuestionHandler.getAllSubQuestions(question));
        }
        cursor.close();
        db.close();
        return question;
    }

    public boolean isContains(String question_id) {
        db = getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT * FROM " + SurveyQuestionsSchema.SurveyQuestionsTable.TABLE_NAME + " where " + SurveyQuestionsSchema.SurveyQuestionsTable.COLUMN_NAME_QUESTIONID + "='" + question_id + "'", null);
        if (cursor.getCount() > 0) {
            return true;
        }
        return false;

    }

    //DELETE
    public boolean deleteQuestion(SurveyQuestion sq) {
        db = getWritableDatabase();
        if (isContains(sq.getQuestion_id())) {
            db.delete(SurveyQuestionsSchema.SurveyQuestionsTable.TABLE_NAME, SurveyQuestionsSchema.SurveyQuestionsTable.COLUMN_NAME_QUESTIONID
                    + " = '" + sq.getQuestion_id() + "'", null);
            db.close();
            if (sq.getOptionItems() != null && !sq.getOptionItems().isEmpty())
                optionItemHandler.deleteAllOption(sq);
            if (sq.getSubQuestions() != null && !sq.getSubQuestions().isEmpty())
                subQuestionHandler.deleteAllSubQuestions(sq);
            return true;
        }
        return false;

    }
    public void deleteQuestionBasedOnSurveyId(String survey_id){
        db = getWritableDatabase();
        db.delete(SurveyQuestionsSchema.SurveyQuestionsTable.TABLE_NAME, SurveyQuestionsSchema.SurveyQuestionsTable.COLUMN_NAME_SURVEYID + " ='"+survey_id+"'", null);
        db.close();
    }



    public void updateQuestion(SurveyQuestion question){
        db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(SurveyQuestionsSchema.SurveyQuestionsTable.COLUMN_NAME_QUESTIONID, question.getQuestion_id());
        values.put(SurveyQuestionsSchema.SurveyQuestionsTable.COLUMN_NAME_PARENTID, question.getParent_id());
        values.put(SurveyQuestionsSchema.SurveyQuestionsTable.COLUMN_NAME_SURVEYID, question.getSurvey_id());
        values.put(SurveyQuestionsSchema.SurveyQuestionsTable.COLUMN_NAME_QUESTIONTYPE, question.getQuestion_type());
        values.put(SurveyQuestionsSchema.SurveyQuestionsTable.COLUMN_NAME_TITLE, question.getTitle());
        values.put(SurveyQuestionsSchema.SurveyQuestionsTable.COLUMN_NAME_QUESTION, question.getQuestion());
        values.put(SurveyQuestionsSchema.SurveyQuestionsTable.COLUMN_NAME_QUESTIONORDER, question.getQuestion_order());
        values.put(SurveyQuestionsSchema.SurveyQuestionsTable.COLUMN_NAME_GID, question.getGid());

        db.update(SurveyQuestionsSchema.SurveyQuestionsTable.TABLE_NAME, values, SurveyQuestionsSchema.SurveyQuestionsTable.COLUMN_NAME_QUESTIONID+ " = '" + question.getQuestion_id() + "'", null);
        db.close();
        if (question.getOptionItems() != null && !question.getOptionItems().isEmpty())
            optionItemHandler.addAllOption(question);
        if (question.getSubQuestions() != null && !question.getSubQuestions().isEmpty())
            subQuestionHandler.addAllSubQuestions(question);
    }

    public int getSize(){
        db = getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT * FROM " + SurveyQuestionsSchema.SurveyQuestionsTable.TABLE_NAME , null);
        return cursor.getCount();
    }
}

