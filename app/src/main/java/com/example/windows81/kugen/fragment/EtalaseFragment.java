package com.example.windows81.kugen.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.example.windows81.kugen.R;
import com.example.windows81.kugen.activity.DetailActivity;
import com.example.windows81.kugen.activity.MainActivity;
import com.example.windows81.kugen.database.DatabaseHandler;
import com.example.windows81.kugen.task.GetQuestionTask;

/**
 * Created by rifkiadrn on 4/23/2018.
 */

public class EtalaseFragment extends Fragment {
    LinearLayout mainLinear;
    private DatabaseHandler dbHandler;
    private GetQuestionTask getQuestion;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_etalase, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();
        this.mainLinear = (LinearLayout) getActivity().findViewById(R.id.mainLinear);
        setSingleEvent(mainLinear);
    }

    private void setSingleEvent(LinearLayout mainLinear) {
        // Loop all child item of Main Linear
        for (int i = 0; i < mainLinear.getChildCount(); i++) {
            // Cast object to CardView
            final CardView cardView = (CardView) mainLinear.getChildAt(i);
            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(getActivity(), DetailActivity.class);
                    intent.putExtra("survey_id", cardView.getTag()+"");
                    startActivity(intent);
                }
            });
        }
    }
}
