package com.example.windows81.kugen.task;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.windows81.kugen.R;
import com.example.windows81.kugen.activity.QuestionActivity;
import com.example.windows81.kugen.activity.ResponseHistoryActivity;
import com.example.windows81.kugen.activity.ScoreActivity;
import com.example.windows81.kugen.adapter.ResponseAdapter;
import com.example.windows81.kugen.database.SavedResponseHandler;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

public class AddResponseTask extends AsyncTask<Object, Object, String> {

    private Context context;
    private String surveyId;
    private ArrayList<JSONObject> arrayInput;
    private boolean isSuccess;
    private int responseId;

    public AddResponseTask(Context context, String survey_id,ArrayList<JSONObject> arrayInput) {
        this.surveyId = survey_id;
        this.context = context;
        this.arrayInput = arrayInput;
        this.isSuccess = false;
        this.responseId = -1;
    }

    @Override
    public String doInBackground(Object... params) {
        DefaultHttpClient client = new DefaultHttpClient();
        String res = "";
        if(params.length == 1){
            this.responseId = Integer.parseInt(params[0]+"");
        }
        try {
            HttpPost post = new HttpPost("https://kuesionergenerik.limequery.com/admin/remotecontrol");
            post.setHeader("Content-type", "application/json");
            post.setEntity(new StringEntity("{\"method\": \"get_session_key\", \"params\": [\"KugenPPL\", \"kugenasoy\"], \"id\": 1}"));
            Log.d("akuini", "");
            try {
                HttpResponse response = client.execute(post);
                if (response.getStatusLine().getStatusCode() == 200) {
                    HttpEntity entity = response.getEntity();
                    String sessionKey = parse(EntityUtils.toString(entity));
                    Log.d("sessionkey", sessionKey);
                    Log.d("input", this.arrayInput.get(0).toString());
                    StringEntity str = new StringEntity("{\"method\": \"add_response\", \"params\": [ "+sessionKey+", "+this.surveyId+", "+this.arrayInput.get(0)+"], \"id\": 1}");
                    Log.d("stringentity", str.toString()+"");
                    post.setEntity(str);
                    response = client.execute(post);
                    JsonElement jsonRes = null;
                    Log.d("status code", ""+response.getStatusLine().getStatusCode());
                    if (response.getStatusLine().getStatusCode() == 200) {
                        entity = response.getEntity();
                        jsonRes = new JsonParser().parse(EntityUtils.toString(entity));
                        Gson gson = new Gson();
                        Log.d("get Response", jsonRes.toString());
                        res = jsonRes.toString();
                        isSuccess = true;
                    }else {
                        Log.d("get surveys error", EntityUtils.toString(entity));
                    }
                }
            } catch (UnknownHostException e){
                res = "Gagal untuk men-submit jawaban, anda tidak memiliki koneksi internet. Jawaban anda akan disimpan ke database lokal, silahkan submit ulang ketika terhubung dengan internet.";
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (UnsupportedEncodingException e){
            e.printStackTrace();
        }
        return res;
    }

    @Override
    public void onPostExecute(String result) {
        Activity mainAct = (Activity) context;
        View spinner = (ProgressBar) mainAct.findViewById(R.id.progress_bar);
        if (this.arrayInput.size() == 2){
            Iterator<?> keys = this.arrayInput.get(1).keys();
            int score = 0;
            while( keys.hasNext() ) {
                String key = (String)keys.next();
                try {
                    score+= Integer.parseInt(this.arrayInput.get(1).get(key)+"");

                } catch (JSONException e){

                }
            }
            spinner.setVisibility(View.GONE);
            if(isSuccess){
                Toast.makeText(context, "Submit success: "+result, Toast.LENGTH_SHORT).show();
            }
            else{
                SavedResponseHandler savedResponseHandler = new SavedResponseHandler(context);
                savedResponseHandler.addResponse(this.surveyId, this.arrayInput.get(0).toString());
                Toast.makeText(context, "Submit failed:  "+result, Toast.LENGTH_SHORT).show();
                Toast.makeText(context, "Saved to SQLite: "+savedResponseHandler.getAllResponse().toString(), Toast.LENGTH_SHORT).show();
            }
            Intent intent = new Intent(context, ScoreActivity.class);
            intent.putExtra("score", score+"");
            intent.putExtra("survey_id",this.surveyId);
            context.startActivity(intent);
        } else {
            spinner.setVisibility(View.GONE);
            if(isSuccess){
                SavedResponseHandler savedResponseHandler = new SavedResponseHandler(context);
                if (this.responseId >=0) {
                    savedResponseHandler.deleteResponse(this.responseId);
                    savedResponseHandler = new SavedResponseHandler(context);
                    Map<Integer, ArrayList<String>> savedResponse = savedResponseHandler.getAllResponse();
                    Log.d("size savedresponse", savedResponse.size() + "");
                    Intent intent = new Intent(mainAct, ResponseHistoryActivity.class);
                    mainAct.startActivity(intent);
                    Toast.makeText(context, "Submit success: " + result, Toast.LENGTH_SHORT).show();
                }
            }
            else{
                Toast.makeText(context, "Submit failed:  "+result, Toast.LENGTH_SHORT).show();
            }
        }

    }

    public static String parse(String jsonLine) {
        JsonElement jelement = new JsonParser().parse(jsonLine);
        Log.d("jelement", jelement.toString());
        JsonObject jobject = jelement.getAsJsonObject();
        String result = jobject.get("result").toString();
        return result;
    }
}
