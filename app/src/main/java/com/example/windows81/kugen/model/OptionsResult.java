package com.example.windows81.kugen.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by rifkiadrn on 3/29/2018.
 */

public class OptionsResult {

    @SerializedName("answeroptions")
    private Map<String,OptionItem> answerOptions;

    public OptionsResult(TreeMap<String, OptionItem> answerOptions) {
        this.answerOptions = answerOptions;
    }

    public Map<String, OptionItem> getAnswerOptions() {
        return answerOptions;
    }
}

