package com.example.windows81.kugen.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.windows81.kugen.R;

/**
 * Created by Adit on 10/04/2018.
 */

public class ScoreActivity extends AppCompatActivity {
    Button surveyLagiButton;
    Button toDashboardButton;
    ProgressBar scoreProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_score);

        surveyLagiButton = (Button) findViewById(R.id.buttonSurvey);
        toDashboardButton = (Button) findViewById(R.id.buttonDashboard);
        scoreProgress = (ProgressBar) findViewById(R.id.progressBar);

        TextView textView = (TextView) findViewById(R.id.details);
        textView.setText(getIntent().getStringExtra("score"));
        Log.d("SCORE",getIntent().getStringExtra("score"));
        scoreProgress.setProgress(Integer.parseInt(getIntent().getStringExtra("score")));
        // set event
        setButtonDashboard(toDashboardButton);
        setButtonSurvey(surveyLagiButton);

    }

    private void setButtonSurvey(Button surveyLagiButton){
        surveyLagiButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ScoreActivity.this, DetailActivity.class);
                intent.putExtra("survey_id", getIntent().getStringExtra("survey_id"));
                startActivity(intent);
            }
        });

    }

    private void setButtonDashboard(Button toDashboardButton){
        toDashboardButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ScoreActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });

    }

}
