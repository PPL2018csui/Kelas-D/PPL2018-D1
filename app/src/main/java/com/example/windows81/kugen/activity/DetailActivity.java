package com.example.windows81.kugen.activity;

import android.content.Intent;
import android.support.design.widget.NavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.windows81.kugen.R;
import com.example.windows81.kugen.task.GetQuestionTask;

public class DetailActivity extends DrawerActivity {
    Button btnSurvey;
    Button btnDownloadSurvey;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        NavigationView navigationView = findViewById(R.id.nav_view);
        MenuItem item = navigationView.getMenu().findItem(R.id.nav_my_survey);
        item.setChecked(true);

        FrameLayout contentFrameLayout = (FrameLayout) findViewById(R.id.content_bookmark); //Remember this is the FrameLayout area within your activity_main.xml
        getLayoutInflater().inflate(R.layout.activity_detail, contentFrameLayout);
        setTitle("Detail Survey");

        this.btnSurvey = (Button) findViewById(R.id.btnSurvey);
        this.btnDownloadSurvey = (Button) findViewById(R.id.btnDownloadSurvey);
        Log.d("btn aw",btnSurvey.getId()+"");

        // Set Button
        setButton (btnSurvey);

        btnDownloadSurvey.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                final View spinner = (ProgressBar) findViewById(R.id.progress_bar2);
                spinner.setVisibility(View.VISIBLE);
                GetQuestionTask getQuestionTask = new GetQuestionTask(DetailActivity.this, getIntent().getStringExtra("survey_id"), true);
                getQuestionTask.execute();
            }
        });

    }

    private void setButton(Button btn) {
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DetailActivity.this, RespondentActivity.class);
                intent.putExtra("survey_id", getIntent().getStringExtra("survey_id"));
                startActivity(intent);
            }
        });
    }
}
