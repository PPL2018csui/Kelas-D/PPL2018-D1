package com.example.windows81.kugen.schema;

import android.provider.BaseColumns;

import com.example.windows81.kugen.database.SavedResponseHandler;

/**
 * Created by rifkiadrn on 4/22/2018.
 */

public class SavedResponseSchema {
    private SavedResponseSchema() {}

    public static class SavedResponseTable implements BaseColumns {
        public static final String TABLE_NAME = "saved_response";
        public static final String COLUMN_NAME_SURVEYID = "survey_id";
        public static final String COLUMN_NAME_RESPONSE = "response";
    }
}
