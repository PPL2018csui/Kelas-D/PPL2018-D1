package com.example.windows81.kugen.model;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Adrian Hartanto on 20/03/2018.
 */

public class SurveyQuestion implements Comparable<SurveyQuestion> {

    @SerializedName("qid")
    String question_id;

    @SerializedName("parent_qid")
    String parent_id;

    @SerializedName("sid")
    String survey_id;

    @SerializedName("type")
    String question_type;

    @SerializedName("title")
    String title;

    @SerializedName("question")
    String question;

    @SerializedName("question_order")
    int question_order;

    @SerializedName("gid")
    int gid;

    public Map<String,OptionItem> optionItems;

    public Map<String,String> subQuestions;

    public static final int TEXT_TYPE=0;
    public static final int RADIO_BUTTON_TYPE=1;
    public static final int CHECK_BOX_MULTIPLE_TYPE=2;


    public SurveyQuestion(){
        this.question_id = "";
        this.parent_id = "";
        this.survey_id = "";
        this.question_type = "";
        this.title = "";
        this.question = "";
        this.question_order = 0;
        this.gid = 0;
        this.subQuestions= null;
        this.optionItems=null;
    }

    public SurveyQuestion(String question_id, String parent_id, String survey_id, String question_type,
                          String title, String question, int question_order, int gid) {
        this.question_id = question_id;
        this.parent_id = parent_id;
        this.survey_id = survey_id;
        this.question_type = question_type;
        this.title = title;
        this.question = question;
        this.question_order = question_order;
        this.gid = gid;
        this.optionItems= null;
        this.subQuestions=null;
    }

    public String getQuestion_id() {
        return question_id;
    }

    public void setQuestion_id(String question_id) {
        this.question_id = question_id;
    }

    public String getParent_id() {
        return parent_id;
    }

    public void setParent_id(String parent_id) {
        this.parent_id = parent_id;
    }

    public String getSurvey_id() {
        return survey_id;
    }

    public void setSurvey_id(String survey_id) {
        this.survey_id = survey_id;
    }

    public String getQuestion_type() {
        return question_type;
    }

    public void setQuestion_type(String question_type) {
        this.question_type = question_type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public int getQuestion_order() {
        return question_order;
    }

    public void setQuestion_order(int question_order) {
        this.question_order = question_order;
    }

    public int getGid() { return gid; }

    public void setGid(int gid) { this.gid = gid; }

    public Map<String,OptionItem> getOptionItems() {
        return this.optionItems;
    }

    public void setOptionItems(Map<String,OptionItem> optionItems) {
        this.optionItems = optionItems;
    }

    public Map<String, String> getSubQuestions() {
        return subQuestions;
    }

    public void setSubQuestions(Map<String, String> subQuestions) {
        this.subQuestions = subQuestions;
    }

    @Override
    public int compareTo(@NonNull SurveyQuestion surveyQuestion) {
        if (this.getGid() - surveyQuestion.getGid() == 0){
            return this.getQuestion_order() - surveyQuestion.getQuestion_order();
        } else {
            return this.getGid() - surveyQuestion.getGid();
        }
    }
}
