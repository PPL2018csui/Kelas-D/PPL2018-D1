package com.example.windows81.kugen.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.windows81.kugen.schema.OptionItemSchema;
import com.example.windows81.kugen.schema.SavedResponseSchema;
import com.example.windows81.kugen.schema.SubQuestionSchema;
import com.example.windows81.kugen.schema.SurveyQuestionsSchema;

import java.io.File;


public class DatabaseHandler extends SQLiteOpenHelper {
    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "kugen.db";

    private String SQL_CREATE_Survey_Question_TABLE = "CREATE TABLE " + SurveyQuestionsSchema.SurveyQuestionsTable.TABLE_NAME + "(" +
            SurveyQuestionsSchema.SurveyQuestionsTable.COLUMN_NAME_QUESTIONID + "  TEXT," +
            SurveyQuestionsSchema.SurveyQuestionsTable.COLUMN_NAME_PARENTID + "  TEXT," +
            SurveyQuestionsSchema.SurveyQuestionsTable.COLUMN_NAME_SURVEYID + "  TEXT," +
            SurveyQuestionsSchema.SurveyQuestionsTable.COLUMN_NAME_QUESTIONTYPE + "  TEXT," +
            SurveyQuestionsSchema.SurveyQuestionsTable.COLUMN_NAME_TITLE + "  TEXT," +
            SurveyQuestionsSchema.SurveyQuestionsTable.COLUMN_NAME_QUESTION + "  TEXT," +
            SurveyQuestionsSchema.SurveyQuestionsTable.COLUMN_NAME_QUESTIONORDER + "  INT," +
            SurveyQuestionsSchema.SurveyQuestionsTable.COLUMN_NAME_GID + "  INT," +
            "PRIMARY KEY (" +
            SurveyQuestionsSchema.SurveyQuestionsTable.COLUMN_NAME_QUESTIONID +
            ", " +
            SurveyQuestionsSchema.SurveyQuestionsTable.COLUMN_NAME_SURVEYID + ")" +
            ")";

    private String SQL_CREATE_Questions_Option_TABLE = "CREATE TABLE " + OptionItemSchema.OptionItemTable.TABLE_NAME + "(" +
            OptionItemSchema.OptionItemTable.COLUMN_NAME_SURVEYID + "  TEXT," +
            OptionItemSchema.OptionItemTable.COLUMN_NAME_QUESTIONID + "  TEXT," +
            OptionItemSchema.OptionItemTable.COLUMN_NAME_OPTIONID + "  TEXT," +
            OptionItemSchema.OptionItemTable.COLUMN_NAME_OPTION + "  TEXT," +
            OptionItemSchema.OptionItemTable.COLUMN_NAME_OPTION_ORDER + "  INT," +
            OptionItemSchema.OptionItemTable.COLUMN_NAME_ASSESSMENT_VALUE + "  INT," +
            "PRIMARY KEY (" +
            OptionItemSchema.OptionItemTable.COLUMN_NAME_SURVEYID +
            ", " +
            OptionItemSchema.OptionItemTable.COLUMN_NAME_QUESTIONID +
            ", " +
            OptionItemSchema.OptionItemTable.COLUMN_NAME_OPTIONID + ")" +
            ")";

    private String SQL_CREATE_SubQuestion_TABLE = "CREATE TABLE " + SubQuestionSchema.SubQuestionTable.TABLE_NAME + "(" +
            SubQuestionSchema.SubQuestionTable.COLUMN_NAME_SURVEYID + "  TEXT," +
            SubQuestionSchema.SubQuestionTable.COLUMN_NAME_QUESTIONID + "  TEXT," +
            SubQuestionSchema.SubQuestionTable.COLUMN_NAME_SUBQUESTIONID + "  TEXT," +
            SubQuestionSchema.SubQuestionTable.COLUMN_NAME_SUBQUESTION + "  TEXT," +
            "PRIMARY KEY (" +
            SubQuestionSchema.SubQuestionTable.COLUMN_NAME_SURVEYID +
            ", " +
            SubQuestionSchema.SubQuestionTable.COLUMN_NAME_QUESTIONID +
            ", " +
            SubQuestionSchema.SubQuestionTable.COLUMN_NAME_SUBQUESTIONID + ")" +
            ")";

    private String SQL_CREATE_SavedResponse_TABLE = "CREATE TABLE " + SavedResponseSchema.SavedResponseTable.TABLE_NAME + "(" +
            SavedResponseSchema.SavedResponseTable._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
            SavedResponseSchema.SavedResponseTable.COLUMN_NAME_SURVEYID + "  TEXT," +
            SavedResponseSchema.SavedResponseTable.COLUMN_NAME_RESPONSE + "  TEXT)";

    private String SQL_DROP_Survey_Question_TABLE = "DROP TABLE IF EXISTS " + SurveyQuestionsSchema.SurveyQuestionsTable.TABLE_NAME;
    private String SQL_DROP_Questions_Option_TABLE = "DROP TABLE IF EXISTS " + OptionItemSchema.OptionItemTable.TABLE_NAME;
    private String SQL_DROP_SubQuestion_TABLE = "DROP TABLE IF EXISTS " + SubQuestionSchema.SubQuestionTable.TABLE_NAME;
    private String SQL_DROP_SavedResponse_TABLE = "DROP TABLE IF EXISTS " + SavedResponseSchema.SavedResponseTable.TABLE_NAME;


    private SQLiteDatabase db;
    private String databasePath;
    public DatabaseHandler(){
        super(null, DATABASE_NAME, null, DATABASE_VERSION);
    }
    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        databasePath = context.getExternalFilesDir(null) + "/" + DATABASE_NAME;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        openDb();
        db.execSQL(SQL_CREATE_Survey_Question_TABLE);
        db.execSQL(SQL_CREATE_Questions_Option_TABLE);
        db.execSQL(SQL_CREATE_SubQuestion_TABLE);
        db.execSQL(SQL_CREATE_SavedResponse_TABLE);
//        db.close();
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //Drop Table if exists
        db.execSQL(SQL_DROP_Survey_Question_TABLE);
        db.execSQL(SQL_DROP_Questions_Option_TABLE);
        db.execSQL(SQL_DROP_SubQuestion_TABLE);
        db.execSQL(SQL_DROP_SavedResponse_TABLE);
        // Create tables again
        onCreate(db);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //Drop Table if exists
        db.execSQL(SQL_DROP_Survey_Question_TABLE);
        db.execSQL(SQL_DROP_Questions_Option_TABLE);
        db.execSQL(SQL_DROP_SubQuestion_TABLE);
        db.execSQL(SQL_DROP_SavedResponse_TABLE);
        // Create tables again
        onCreate(db);
    }

    private boolean checkTable(String tableName) {
        String sql = "SELECT question_id FROM " + tableName;
        if (db.isOpen()) {
            try {
                Cursor cur = db.rawQuery(sql, null);
                cur.close();
                return true;
            } catch (SQLiteException e) {
                return false;
            }
        } else return false;
    }

    private boolean checkDb() {
        File db = new File(databasePath);
        return db.exists();
    }

    private void openDb() {
        if (!checkDb())
            db = SQLiteDatabase.openOrCreateDatabase(databasePath, null);
        else
            db = SQLiteDatabase.openDatabase(databasePath, null, SQLiteDatabase.OPEN_READWRITE);

        if (!checkTable(SurveyQuestionsSchema.SurveyQuestionsTable.TABLE_NAME)) {
            db.execSQL(SQL_CREATE_Survey_Question_TABLE);
        } else
            Log.d("DEBUG", "Survey Question Table exists");

        if (!checkTable(OptionItemSchema.OptionItemTable.TABLE_NAME)) {
            db.execSQL(SQL_CREATE_Questions_Option_TABLE);
        } else
            Log.d("DEBUG", "Questions Option Table exists");

        if (!checkTable(SubQuestionSchema.SubQuestionTable.TABLE_NAME)) {
            db.execSQL(SQL_CREATE_SubQuestion_TABLE);
        } else
            Log.d("DEBUG", "Sub Question Table exists");

        if (!checkTable(SavedResponseSchema.SavedResponseTable.TABLE_NAME)) {
            db.execSQL(SQL_CREATE_SavedResponse_TABLE);
        } else
            Log.d("DEBUG", "Saved Response Table exists");

    }

    protected SQLiteDatabase getDatabase() {
        return db;
    }

    protected String getDatabasePath() {
        return databasePath;
    }

    public int getDatabaseVersion() {
        return DATABASE_VERSION;
    }
}
