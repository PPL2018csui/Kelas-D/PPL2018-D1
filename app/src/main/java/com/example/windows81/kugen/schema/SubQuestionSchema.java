package com.example.windows81.kugen.schema;

import android.provider.BaseColumns;

import com.example.windows81.kugen.model.SubQuestion;

/**
 * Created by rifkiadrn on 4/21/2018.
 */

public class SubQuestionSchema {
    private SubQuestionSchema() {}

    public static class SubQuestionTable implements BaseColumns {
        public static final String TABLE_NAME = "sub_question";
        public static final String COLUMN_NAME_SURVEYID = "survey_id";
        public static final String COLUMN_NAME_QUESTIONID = "question_id";
        public static final String COLUMN_NAME_SUBQUESTIONID = "subquestion_id";
        public static final String COLUMN_NAME_SUBQUESTION = "subquestion";
    }
}
