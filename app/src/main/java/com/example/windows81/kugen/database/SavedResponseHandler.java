package com.example.windows81.kugen.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.windows81.kugen.schema.SavedResponseSchema;

import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by rifkiadrn on 4/22/2018.
 */

public class SavedResponseHandler extends DatabaseHandler {
    private SQLiteDatabase db;


    public SavedResponseHandler(Context context){
        super(context);
        this.db = super.getDatabase();
    }
    public void addResponse(String survey_id, String response){
        db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(SavedResponseSchema.SavedResponseTable.COLUMN_NAME_SURVEYID, survey_id);
        values.put(SavedResponseSchema.SavedResponseTable.COLUMN_NAME_RESPONSE, response);

        db.insert(SavedResponseSchema.SavedResponseTable.TABLE_NAME, null, values);
    }
    public Map<Integer,ArrayList<String>> getAllResponse(){
        db = getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT * FROM " + SavedResponseSchema.SavedResponseTable.TABLE_NAME + " ORDER BY " + SavedResponseSchema.SavedResponseTable._ID + " ASC", null);

        Map<Integer,ArrayList<String>> responseList = new TreeMap<>();

        // Traversing through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                ArrayList<String> response = new ArrayList<>();
                response.add(cursor.getString(cursor.getColumnIndex(SavedResponseSchema.SavedResponseTable.COLUMN_NAME_SURVEYID)));
                response.add(cursor.getString(cursor.getColumnIndex(SavedResponseSchema.SavedResponseTable.COLUMN_NAME_RESPONSE)));
                int id = cursor.getInt(cursor.getColumnIndex(SavedResponseSchema.SavedResponseTable._ID));
                responseList.put(id, response);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        // return question list
        return responseList;
    }
    public Map<Integer,ArrayList<String>> getResponsesBySurveyId(String survey_id){
        db = getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT * FROM " + SavedResponseSchema.SavedResponseTable.TABLE_NAME +" WHERE "+SavedResponseSchema.SavedResponseTable.COLUMN_NAME_SURVEYID+ "='"+survey_id+"'"+ " ORDER BY " + SavedResponseSchema.SavedResponseTable._ID + " ASC", null);

        Map<Integer,ArrayList<String>> responseList = new TreeMap<>();

        // Traversing through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                ArrayList<String> response = new ArrayList<>();
                response.add(cursor.getString(cursor.getColumnIndex(SavedResponseSchema.SavedResponseTable.COLUMN_NAME_SURVEYID)));
                response.add(cursor.getString(cursor.getColumnIndex(SavedResponseSchema.SavedResponseTable.COLUMN_NAME_RESPONSE)));
                int id = cursor.getInt(cursor.getColumnIndex(SavedResponseSchema.SavedResponseTable._ID));
                responseList.put(id, response);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return responseList;
    }
    public void deleteAllResponse(){
        db = getWritableDatabase();
        db.delete(SavedResponseSchema.SavedResponseTable.TABLE_NAME, "", null);
        db.close();
    }
    public void deleteResponse(int id){
        db = getWritableDatabase();
        if (isContains(id)) {
            db.delete(SavedResponseSchema.SavedResponseTable.TABLE_NAME, SavedResponseSchema.SavedResponseTable._ID +"='" + id + "'", null);
            db.close();
        }
    }
    public boolean isContains(int id){
        db = getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT * FROM " + SavedResponseSchema.SavedResponseTable.TABLE_NAME +" WHERE "+ SavedResponseSchema.SavedResponseTable._ID +"='" + id + "'", null);
        if (cursor.getCount() > 0) {
            return true;
        }
        return false;
    }
}