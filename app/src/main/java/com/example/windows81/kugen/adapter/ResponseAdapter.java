package com.example.windows81.kugen.adapter;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.windows81.kugen.R;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created by rifkiadrn on 4/23/2018.
 */

public class ResponseAdapter extends BaseAdapter {
    private final LayoutInflater mInflater;
    private Map<Integer, ArrayList<String>> responseList;
    private Integer[] mapKeys;


    public ResponseAdapter(@NonNull Context context, @LayoutRes int resource, Map<Integer, ArrayList<String>> items){
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.responseList = items;
        this.mapKeys = responseList.keySet().toArray(new Integer[items.size()]);
    }

    @Override
    public int getCount() {
        return responseList.size();
    }

    @Override
    public ArrayList<String> getItem(int position) {
        return responseList.get(mapKeys[position]);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LinearLayout layout;
        int key = mapKeys[position];
        ArrayList<String> values = getItem(position);
        if (convertView == null) {
            layout = (LinearLayout) mInflater.inflate(R.layout.responsehistory_item, parent, false);
        } else {
            layout = (LinearLayout) convertView;
        }
        layout.setId(key);
        setText(layout, values);
        return layout;
    }

    public void setText(LinearLayout layout, ArrayList<String> position) {
        TextView survey_id = (TextView) layout.findViewById(R.id.survey_id);
        TextView response = (TextView) layout.findViewById(R.id.response);

        survey_id.setText(position.get(0));
        response.setText(position.get(1));
    }
}
