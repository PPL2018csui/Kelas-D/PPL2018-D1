package com.example.windows81.kugen.model;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import java.util.Map;
import java.util.TreeMap;

/**
 * Created by rifkiadrn on 4/17/2018.
 */

public class SubQuestionTest {
    SubQuestion subQuestion;

    @Before
    public void SetUp(){
        TreeMap<String,String> subQuestionsMap = new TreeMap<>();
        subQuestionsMap.put("Test", "Data");
        subQuestion = new SubQuestion(subQuestionsMap);

    }

    @Test
    public void testGetSubQuestions(){
        assertEquals("Data", subQuestion.getSubQuestions().get("Test"));
    }

    @Test
    public void testIsSelected(){
        assertFalse(subQuestion.isSelected());
    }

    @Test
    public void testSetSelected(){
        subQuestion.setSelected(true);
        assertTrue(subQuestion.isSelected());
    }
}
