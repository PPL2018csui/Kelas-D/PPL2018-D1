package com.example.windows81.kugen.model;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

/**
 * Created by rifkiadrn on 4/17/2018.
 */

public class OptionItemTest {

    String optionAnswer;
    int order;

    String optionAnswer_reset;
    int order_reset;

    OptionItem optionItem;
    @Before
    public void SetUp(){
        optionAnswer = "Benar";
        order = 1;

        optionAnswer_reset = "Salah";
        order_reset = 2;
        optionItem = new OptionItem(optionAnswer, order);
    }

    @Test
    public void testGetOptionAnswer() {
        assertEquals(optionAnswer,optionItem.getOptionAnswer());
    }

    @Test
    public void testSetOptionAnswer() {
        optionItem.setOptionAnswer(optionAnswer_reset);
        assertEquals(optionAnswer_reset, optionItem.getOptionAnswer());
    }

    @Test
    public void testGetOrder() {
        assertEquals(order, optionItem.getOrder());
    }

    @Test
    public void testSetOrder() {
        optionItem.setOrder(order_reset);
        assertEquals(order_reset, optionItem.getOrder());
    }

    @Test
    public void testIsSelected(){
        assertFalse(optionItem.isSelected());
    }

    @Test
    public void testSetSelected(){
        optionItem.setSelected(true);
        assertTrue(optionItem.isSelected());
    }
}
