package com.example.windows81.kugen.database;

import com.example.windows81.kugen.BuildConfig;
import com.google.gson.JsonParser;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import java.util.ArrayList;
import java.util.Map;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;

/**
 * Created by rifkiadrn on 4/22/2018.
 */

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class SavedResponseHandlerTest {
    SavedResponseHandler savedResponseHandler;
    @Before
    public void setUp(){
        this.savedResponseHandler = new SavedResponseHandler(RuntimeEnvironment.application);
        String response = "{\"482498X2X2\":\"2\",\"482498X2X31\":\"Ajar\",\"482498X2X32\":\"Baca\",\"482498X2X12\":\"haha\",\"482498X2X11\":\"hihi\",\"482498X2X18\":\"TNI\",\"482498X2X19\":\"huhu\",\"482498X2X20\":\"500\"}";
        String response2 = "{\"482497X2X2\":\"3\",\"482497X2X31\":\"Ajir\",\"482497X2X32\":\"Baca\",\"482497X2X12\":\"haha\",\"482497X2X11\":\"hihi\",\"482497X2X18\":\"TNI\",\"482497X2X19\":\"huhu\",\"482497X2X20\":\"500\"}";

        String survey_id = "482498";
        String survey_id2 = "482497";
        savedResponseHandler.addResponse(survey_id, response);
        savedResponseHandler.addResponse(survey_id2, response2);
    }

    @Test
    public void testGetAllResponse(){
        assertEquals(2, savedResponseHandler.getAllResponse().size());
    }

    @Test
    public void testGetResponsesBySurveyId(){
        assertEquals(1, savedResponseHandler.getResponsesBySurveyId("482498").size());
    }

    @Test
    public void testDeleteAllResponse(){
        savedResponseHandler.deleteAllResponse();
        assertEquals(0, savedResponseHandler.getAllResponse().size());
    }

    @Test
    public void testDeleteResponse(){
        savedResponseHandler.deleteResponse(2);
        assertEquals(1, savedResponseHandler.getAllResponse().size());
    }

    @Test
    public void testIsContains(){
        assertTrue(savedResponseHandler.isContains(1));
        assertFalse(savedResponseHandler.isContains(5));
    }
}
