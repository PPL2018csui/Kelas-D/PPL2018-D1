package com.example.windows81.kugen.database;

import com.example.windows81.kugen.BuildConfig;
import com.example.windows81.kugen.model.SubQuestion;
import com.example.windows81.kugen.model.SurveyQuestion;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import java.util.Map;
import java.util.TreeMap;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static org.junit.Assert.assertNotEquals;

/**
 * Created by rifkiadrn on 4/21/2018.
 */

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class SubQuestionHandlerTest {
    SubQuestionHandler subQuestionHandler;
    SurveyQuestion a;

    @Before
    public void setUp(){
        subQuestionHandler = new SubQuestionHandler(RuntimeEnvironment.application);
        a = new SurveyQuestion("1", "0", "735376", "T", "SDS1", "Apa ya?", 1, 2);
        Map<String, String> subQuestions = new TreeMap<>();
        subQuestions.put("A1", "Bekerja?");
        subQuestions.put("A2", "Belajar?");
        subQuestions.put("A3", "Bersama?");
        a.setSubQuestions(subQuestions);
        subQuestionHandler.addAllSubQuestions(a);
    }

    @After
    public void tearDown(){
        subQuestionHandler.close();
    }

    @Test
    public void testGetAllSubQuestions() {
        assertEquals(3, subQuestionHandler.getAllSubQuestions(a).size());
        assertNotEquals(5, subQuestionHandler.getAllSubQuestions(a).size());
    }

    @Test
    public void testGetSubQuestion() {
        assertEquals("Bekerja?", subQuestionHandler.getSubQuestion(a.getSurvey_id(),a.getQuestion_id(),a.getSubQuestions().entrySet().iterator().next().getKey()));
        assertNotEquals("Bekerjaaaa?", subQuestionHandler.getSubQuestion(a.getSurvey_id(),a.getQuestion_id(),a.getSubQuestions().entrySet().iterator().next().getKey()));
    }

    @Test
    public void testIsContains() {
        assertTrue(subQuestionHandler.isContains(a.getSurvey_id(),a.getQuestion_id(),a.getSubQuestions().entrySet().iterator().next().getKey()));
        assertFalse(subQuestionHandler.isContains(a.getSurvey_id(),a.getQuestion_id(),"500"));
    }

    @Test
    public void testDeleteAllSubQuestion() {
        subQuestionHandler.deleteAllSubQuestions(a);
        assertEquals(0,subQuestionHandler.getAllSubQuestions(a).size());
    }

    @Test
    public void testUpdateSubQuestion(){
        subQuestionHandler.updateSubQuestion(a.getSurvey_id(),a.getQuestion_id(),a.getSubQuestions().entrySet().iterator().next().getKey(), "Baru bekerja?");
        String firstOption = subQuestionHandler.getSubQuestion(a.getSurvey_id(),a.getQuestion_id(),a.getSubQuestions().entrySet().iterator().next().getKey());
        assertEquals("Baru bekerja?", firstOption);
        assertNotEquals("Opt-1", firstOption);
    }
}
