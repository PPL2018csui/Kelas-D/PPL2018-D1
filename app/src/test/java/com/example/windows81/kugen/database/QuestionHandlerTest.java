package com.example.windows81.kugen.database;

import com.example.windows81.kugen.BuildConfig;
import com.example.windows81.kugen.model.SurveyQuestion;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;
import static org.junit.Assert.assertNotEquals;

/**
 * Created by rifkiadrn on 3/26/2018.
 */

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class QuestionHandlerTest {
    QuestionHandler questionHandler;
    SurveyQuestion a,b,c,d;

    @Before
    public void setUp(){
        questionHandler = new QuestionHandler(RuntimeEnvironment.application);
        a = new SurveyQuestion("1", "0", "735376", "T", "SDS1", "Apa ya?", 1, 2);
        b = new SurveyQuestion("2", "0", "735376", "T", "SDS2", "Apa yang membuat?", 2, 2);
        c = new SurveyQuestion("3", "0", "735376", "T", "SDS3", "apa iya?", 3,2);
        d = new SurveyQuestion("3", "0", "735376", "T", "SDS4", "apa iya?", 3,2);

        List<SurveyQuestion> listQuestion = new ArrayList<>();
        listQuestion.add(a);
        listQuestion.add(b);
        listQuestion.add(c);
        listQuestion.add(d);

        questionHandler.addAllQuestions(listQuestion);
    }

    @After
    public void tearDown(){

        questionHandler.close();
    }

    @Test
    public void questionTest(){
        assertNotNull(questionHandler.getAllQuestions("735376"));
        List<SurveyQuestion> dariDatabase = questionHandler.getAllQuestions("735376");
        assertEquals("1", dariDatabase.get(0).getQuestion_id());
        assertEquals("2", dariDatabase.get(1).getQuestion_id());
        assertEquals("3", dariDatabase.get(2).getQuestion_id());
    }

    @Test
    public void testGetQuestion(){
        SurveyQuestion question = questionHandler.getQuestion("1");
        assertNotNull(question);
    }

    @Test
    public void testIsContains(){
        Boolean temp = questionHandler.isContains("2");
        assertTrue(temp);
    }

    @Test
    public void testDeleteQuestion(){
        SurveyQuestion question = new SurveyQuestion("5", "0", "735376", "T", "SDS5", "apa iya begitukah?", 4, 2);
        questionHandler.addQuestion(question);
        questionHandler.deleteQuestion(question);
        assertEquals(3, questionHandler.getSize());

        SurveyQuestion questionFail = new SurveyQuestion("100", "0", "735376", "T", "SDS5", "apa iya begitukah?", 4, 2);
        questionHandler.addQuestion(questionFail);
        questionHandler.deleteQuestion(question);
        assertNotEquals(3, questionHandler.getSize());
    }

    @Test
    public void testUpdateQuestion(){
        SurveyQuestion question = new SurveyQuestion("3", "0", "735376", "T", "SDS3", "apa iya yaah?", 3, 2);
        questionHandler.updateQuestion(question);
        assertEquals(question.getQuestion(), questionHandler.getQuestion("3").getQuestion());
    }

    @Test
    public void testGetSize(){
        assertEquals(3, questionHandler.getSize());
    }

}
