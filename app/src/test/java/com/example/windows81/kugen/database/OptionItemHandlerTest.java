package com.example.windows81.kugen.database;

import com.example.windows81.kugen.BuildConfig;
import com.example.windows81.kugen.model.OptionItem;
import com.example.windows81.kugen.model.SurveyQuestion;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import java.util.Map;
import java.util.TreeMap;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static org.junit.Assert.assertNotEquals;

/**
 * Created by rifkiadrn on 4/21/2018.
 */

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class OptionItemHandlerTest {
    OptionItemHandler optionItemHandler;
    SurveyQuestion a;

    @Before
    public void setUp(){
        optionItemHandler = new OptionItemHandler(RuntimeEnvironment.application);
        a = new SurveyQuestion("1", "0", "735376", "T", "SDS1", "Apa ya?", 1, 2);
        Map<String, OptionItem> optionItems = new TreeMap<>();
        optionItems.put("1", new OptionItem("Opt-1", 1));
        optionItems.put("2", new OptionItem("Opt-2", 2));
        optionItems.put("3", new OptionItem("Opt-3", 3));
        a.setOptionItems(optionItems);
        optionItemHandler.addAllOption(a);
    }

    @After
    public void tearDown(){

        optionItemHandler.close();
    }

    @Test
    public void testGetAllOption() {
        assertEquals(3, optionItemHandler.getAllOption(a).size());
        assertNotEquals(5, optionItemHandler.getAllOption(a).size());
    }

    @Test
    public void testGetOption() {
        assertEquals("Opt-1", optionItemHandler.getOption(a.getSurvey_id(),a.getQuestion_id(),a.getOptionItems().entrySet().iterator().next().getKey()).getOptionAnswer());
        assertNotEquals("Opt-5", optionItemHandler.getOption(a.getSurvey_id(),a.getQuestion_id(),a.getOptionItems().entrySet().iterator().next().getKey()).getOptionAnswer());
    }

    @Test
    public void testIsContains() {
        assertTrue(optionItemHandler.isContains(a.getSurvey_id(),a.getQuestion_id(),a.getOptionItems().entrySet().iterator().next().getKey()));
        assertFalse(optionItemHandler.isContains(a.getSurvey_id(),a.getQuestion_id(),"500"));
    }

    @Test
    public void testDeleteAllOption() {
        optionItemHandler.deleteAllOption(a);
        assertEquals(0,optionItemHandler.getAllOption(a).size());

    }

    @Test
    public void testUpdateOption(){
        optionItemHandler.updateOption(a.getSurvey_id(),a.getQuestion_id(),a.getOptionItems().entrySet().iterator().next().getKey(), new OptionItem("Opt-1 new", 1));
        String firstOption = optionItemHandler.getOption(a.getSurvey_id(),a.getQuestion_id(),a.getOptionItems().entrySet().iterator().next().getKey()).getOptionAnswer();
        assertEquals("Opt-1 new", firstOption);
        assertNotEquals("Opt-1", firstOption);
    }
}
