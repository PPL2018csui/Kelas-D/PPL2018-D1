package com.example.windows81.kugen.model;

import static junit.framework.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import java.util.Map;
import java.util.TreeMap;

/**
 * Created by rifkiadrn on 4/17/2018.
 */

public class OptionsResultTest {

    OptionsResult optionsResult;
    TreeMap<String,OptionItem> answerOptions;

    @Before
    public void SetUp() {
        answerOptions = new TreeMap<>();
        OptionItem optionItem = new OptionItem("Bekerja", 1);
        answerOptions.put("1",optionItem);
        optionsResult = new OptionsResult(answerOptions);
    }

    @Test
    public void testGetAnswerOptions() {
        assertEquals("Bekerja", optionsResult.getAnswerOptions().get("1").getOptionAnswer());
    }

}
