package com.example.windows81.kugen.model;

import org.junit.Before;
import org.junit.Test;
import org.robolectric.annotation.Config;

import java.util.Map;
import java.util.TreeMap;

import static junit.framework.Assert.assertEquals;
import static org.junit.Assert.*;

/**
 * Created by Adrian Hartanto on 20/03/2018.
 */

public class SurveyQuestionTest {
    String question_id;
    String parent_id;
    String survey_id;
    String question_type;
    String title;
    String question;
    int question_order;
    int gid;

    String question_idReset;
    String parent_idReset;
    String survey_idReset;
    String question_typeReset;
    String titleReset;
    String questionReset;
    int question_orderReset;
    int gid_reset;

    SurveyQuestion surveyQuestion;
    OptionItem optionItem;
    Map<String, OptionItem> optionItems = new TreeMap<>();

    @Before
    public void setUp(){
        question_id = "2";
        parent_id = "0";
        survey_id = "482498";
        question_type = "5";
        title = "pointChoice";
        question = "This is 5 point choice";
        question_order = 1;
        gid = 2;

        question_idReset = "3";
        parent_idReset = "0";
        survey_idReset = "482498";
        question_typeReset = "F";
        titleReset = "array";
        questionReset = "This is array question";
        question_orderReset = 2;
        gid_reset = 3;

        surveyQuestion = new SurveyQuestion(question_id, parent_id, survey_id, question_type, title,
                question, question_order, gid);
        optionItem = new OptionItem("Bekerja", 1);
        optionItems.put("1",optionItem);
        surveyQuestion.setOptionItems(optionItems);
    }

    @Test
    public void testConstructor() {
        assertNotNull(surveyQuestion);
        SurveyQuestion surveyQuestionNull = new SurveyQuestion();
        assertNotNull(surveyQuestionNull);
    }

    @Test
    public void testGetQuestionID(){
        assertEquals(question_id, surveyQuestion.getQuestion_id());
    }

    @Test
    public void testGetParentId(){
        assertEquals(parent_id, surveyQuestion.getParent_id());
    }

    @Test
    public void testGetSurveyId(){
        assertEquals(survey_id, surveyQuestion.getSurvey_id());
    }

    @Test
    public void testGetQuetionType(){
        assertEquals(question_type, surveyQuestion.getQuestion_type());
    }

    @Test
    public void testGetTitle(){
        assertEquals(title, surveyQuestion.getTitle());
    }

    @Test
    public void testGetGid(){
        assertEquals(gid, surveyQuestion.getGid());
    }

    @Test
    public void testGetQuestion(){
        assertEquals(question, surveyQuestion.getQuestion());
    }

    @Test
    public void testGetQuestionOrder(){
        assertEquals(question_order, surveyQuestion.getQuestion_order());
    }

    @Test
    public void testSetQuestionID(){
        surveyQuestion.setQuestion_id(question_idReset);
        assertEquals(question_idReset, surveyQuestion.getQuestion_id());
    }

    @Test
    public void testSetParentId(){
        surveyQuestion.setParent_id(parent_idReset);
        assertEquals(parent_idReset, surveyQuestion.getParent_id());
    }

    @Test
    public void testSetSurveyId(){
        surveyQuestion.setSurvey_id(survey_idReset);
        assertEquals(survey_idReset, surveyQuestion.getSurvey_id());
    }

    @Test
    public void testSetGid(){
        surveyQuestion.setGid(gid_reset);
        assertEquals(gid_reset, surveyQuestion.getGid());
    }

    @Test
    public void testSetQuestionType(){
        surveyQuestion.setQuestion_type(question_typeReset);
        assertEquals(question_typeReset, surveyQuestion.getQuestion_type());
    }

    @Test
    public void testSetTitle(){
        surveyQuestion.setTitle(titleReset);
        assertEquals(titleReset, surveyQuestion.getTitle());
    }

    @Test
    public void testSetQuestion(){
        surveyQuestion.setQuestion(questionReset);
        assertEquals(questionReset, surveyQuestion.getQuestion());
    }

    @Test
    public void testSetQuestionOrder(){
        surveyQuestion.setQuestion_order(question_orderReset);
        assertEquals(question_orderReset, surveyQuestion.getQuestion_order());
    }

    @Test
    public void testGetOptionItems() {
        assertEquals("Bekerja", surveyQuestion.getOptionItems().get("1").getOptionAnswer());
    }

//    @Test
//    public void testSetOptionItems(){
//        Map<String, OptionItem> newOptions = new TreeMap<>();
//        newOptions.put("1", new OptionItem("Belajar", 1));
//        surveyQuestion.setOptionItems(newOptions);
//        assertEquals("Belajar", this.optionItems.get("1").getOptionAnswer());
//    }

    @Test
    public void testSetSubQuestions() {
        surveyQuestion.setSubQuestions(new TreeMap<String, String>());
        assertEquals(0,surveyQuestion.getSubQuestions().size());
    }
}
