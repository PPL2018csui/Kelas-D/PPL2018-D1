package com.example.windows81.kugen;

import android.support.test.rule.ActivityTestRule;
import android.support.v7.widget.RecyclerView;

import com.example.windows81.kugen.activity.MainActivity;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.doesNotExist;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.Visibility.VISIBLE;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withEffectiveVisibility;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

/**
 * Created by rifkiadrn on 4/18/2018.
 */

public class QuestionAdapterInstrumentedTest {

    RecyclerView recyclerView;

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(MainActivity.class);

    @Before
    public void setUp(){
        MainActivity act = mActivityRule.getActivity();
        recyclerView = (RecyclerView) act.findViewById(R.id.recycler_view);
    }

    // Test listview news is exist
    @Test
    public void listviewBookmarkIsExist() {
        onView(withId(R.id.recycler_view)).check(matches(withEffectiveVisibility(VISIBLE)));
        onView(withId(R.id.recycler_view)).check(matches(isDisplayed()));
    }
}
