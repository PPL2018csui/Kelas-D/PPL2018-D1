package com.example.windows81.kugen;

import android.app.Activity;
import android.app.Instrumentation;
import android.support.test.espresso.Espresso;
import android.support.test.rule.ActivityTestRule;

import com.example.windows81.kugen.activity.DetailActivity;
import com.example.windows81.kugen.activity.QuestionActivity;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static android.support.test.InstrumentationRegistry.getInstrumentation;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.junit.Assert.*;

/**
 * Created by User on 20-Mar-18.
 */
public class DetailActivityTest {
    @Rule
    public ActivityTestRule<DetailActivity> detailActivityTestRule = new ActivityTestRule<DetailActivity>(DetailActivity.class);

    private DetailActivity mActivity = null;

    Instrumentation.ActivityMonitor monitor = getInstrumentation().addMonitor(QuestionActivity.class.getName(), null, false);

    @Before
    public void setUp() throws Exception {
        mActivity = detailActivityTestRule.getActivity();
    }

    @Test
    public void TestLaunchOnActivityClick() {
        assertNotNull(mActivity.findViewById(R.id.btnSurvey));
        Espresso.onView(withId(R.id.btnSurvey)).perform(click());
        Activity questionnaireActivity = getInstrumentation().waitForMonitorWithTimeout(monitor, 5000);
        assertNotNull(questionnaireActivity);
        questionnaireActivity.finish();

    }

    @After
    public void tearDown() throws Exception {
        mActivity = null;
    }

}