package com.example.windows81.kugen;


import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.example.windows81.kugen.activity.DrawerActivity;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

/**
 * Created by harryakbar on 3/18/18.
 */
@RunWith(AndroidJUnit4.class)
public class DrawerActivityTest {
    @Rule
    public ActivityTestRule<DrawerActivity> mMainActivityTestRule =
            new ActivityTestRule<>(DrawerActivity.class);

    @Test
    public void navigationDrawerTest() {
        onView(withId(R.id.drawer_layout))
                .check(matches(isDisplayed()));
    }

    @Test
    public void onBackPressedTest() {
        onView(withId(R.id.drawer_layout))
                .check(matches(isDisplayed()));
    }

    @Test
    public void onOptionsItemSelectedTest() {
        onView(withId(R.id.drawer_layout))
                .check(matches(isDisplayed()));
    }
}