package com.example.windows81.kugen.activity;

import android.support.test.rule.ActivityTestRule;
import android.view.View;

import com.example.windows81.kugen.R;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Windows 8.1 on 4/15/2018.
 */
public class SpeechActivityTest {
    @Rule
    public ActivityTestRule<RespondentActivity> mActivityTestRule = new ActivityTestRule<RespondentActivity>(RespondentActivity.class);

    private RespondentActivity mActivity = null;

    @Before
    public void setUp() throws Exception {
        mActivity = mActivityTestRule.getActivity();
    }

    @Test
    public void testButtonMicLaunch(){
        View v = mActivity.findViewById(R.id.btn_record_audio);

        assertNotNull(v);
    }

    @Test
    public void testViewOutputLaunch(){
        View v = mActivity.findViewById(R.id.record_status);

        assertNotNull(v);
    }

    @After
    public void tearDown() throws Exception {
        mActivity = null;
    }

}
