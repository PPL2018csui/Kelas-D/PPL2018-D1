package com.example.windows81.kugen;

import android.app.Activity;
import android.app.Instrumentation;
import android.support.test.espresso.Espresso;
import android.support.test.rule.ActivityTestRule;

import com.example.windows81.kugen.activity.DetailActivity;
import com.example.windows81.kugen.activity.MainActivity;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static android.support.test.InstrumentationRegistry.getInstrumentation;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.junit.Assert.*;

public class MainActivityTest {
    @Rule
    public ActivityTestRule<MainActivity> mainActivityTestRule = new ActivityTestRule<MainActivity>(MainActivity.class);

    private MainActivity mActivity = null;

    Instrumentation.ActivityMonitor monitor = getInstrumentation().addMonitor(DetailActivity.class.getName(), null, false);

    @Before
    public void setUp() throws Exception {
        mActivity = mainActivityTestRule.getActivity();
    }

    @Test
    public void TestCardViewClick() {
        assertNotNull(mActivity.findViewById(R.id.cardView1));
        Espresso.onView(withId(R.id.cardView1)).perform(click());
        Activity detailActivity = getInstrumentation().waitForMonitorWithTimeout(monitor, 5000);
        assertNotNull(detailActivity);
        detailActivity.finish();
    }

    @After
    public void tearDown() throws Exception {
        mActivity = null;
    }
}