package com.example.windows81.kugen;

import android.app.Instrumentation;
import android.support.test.espresso.ViewAction;
import android.support.test.espresso.action.ViewActions;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.example.windows81.kugen.activity.RespondentActivity;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.InstrumentationRegistry.getInstrumentation;
import static android.support.test.espresso.Espresso.closeSoftKeyboard;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isChecked;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.*;

/**
 * Created by harryakbar on 3/18/18.
 */
@RunWith(AndroidJUnit4.class)
public class RespondentActivityTest {
    private String nameToBetyped, ageToBetyped, addressToBetyped, educationToBetyped;

    @Rule
    public ActivityTestRule<RespondentActivity> respondentActivityTestRule = new ActivityTestRule<RespondentActivity>(RespondentActivity.class);
    private RespondentActivity mActivity = null;
    Instrumentation.ActivityMonitor monitor = getInstrumentation().addMonitor(RespondentActivity.class.getName(), null, false);

    @Before
    public void setUp() throws Exception {
        nameToBetyped = "Harry";
        ageToBetyped = "21";
        addressToBetyped = "sfs";
        educationToBetyped= "UI";
        mActivity = respondentActivityTestRule.getActivity();
    }

    @Test
    public void testChangeText() {
        onView(withId(R.id.respondent_name))
                .perform(typeText(nameToBetyped), ViewActions.closeSoftKeyboard());
        onView(withId(R.id.respondent_age))
                .perform(typeText(ageToBetyped), ViewActions.closeSoftKeyboard());
        onView(withId(R.id.respondent_man))
                .perform(click());
        onView(withId(R.id.respondent_man))
                .check(matches(isChecked()));
        onView(withId(R.id.respondent_woman))
                .check(matches(not(isChecked())));
        onView(withId(R.id.respondent_address))
                .perform(typeText(addressToBetyped),  ViewActions.closeSoftKeyboard());
        onView(withId(R.id.respondent_education))
                .perform(typeText(educationToBetyped),  ViewActions.closeSoftKeyboard());

        onView(withId(R.id.btn_submit)).perform(click());
    }

    @After
    public void tearDown() throws Exception {
        mActivity = null;
    }
}